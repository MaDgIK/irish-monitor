# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

*For each release, use the following subsections:*

- *Added (for new features)*
- *Changed (for changes in existing functionality)*
- *Deprecated (for soon-to-be removed features)*
- *Removed (for now removed features)*
- *Fixed (for any bug fixes)*
- *Security (in case of vulnerabilities)*

## v1.0.3 - 30.10.2024
### Changed
Open Access Indicator changed to show open access for the last full year. (e.g. 2023)

## v1.0.2 - 18.09.2024
### Changed
Texts in home and about.

### Added
Add info for publicly-funded filter.
Add engagement tag for engagement-training page.

## v1.0.1 - 25.07.2024
### Fixed
Problem with ORCID KPIs regarding a researcher.
Reordered action buttons in a search page.
Fix import/export indicators tab title wasn't imported.
Fix the redirection URL after verification of a manager.

### Changed
Extract a stakeholder in every field of a json in a chart.
Improvements regarding import/export indicators and editing an indicator.

## v1.0.0 - 26.06.2024 - Official Release
### Added
Add banner for announcements. Disabled by default.
Add guide text in researcher search bar for more accurate results.

### Removed
Remove final release text from footer.

### Changed
Improvements in User Interface of browse RPOs/RFOs pages.

## v0.1.5 - 19.6.2024
### Added
Add open access indicator in browse pages.
Add OAI PMH in user actions page.

### Changed
Change api call to get manage stakeholders from the API.

## v0.1.4 - 03.6.2024
### Added
Add message for results that are not related to Ireland in detailed page of the result.
Add Tab title in indicator form (admin).

### Changed
Position of buttons for multi indicator in cards and add allows for overflow.
_Open Access_ to _Open Access with Licence_ and set precision of the percentage to one decimal.

## v0.1.3 - 20.5.2024
### Added
Share OA Indicator in Monitor pages.
Dashboard Managers Documentation in User Actions.

### Fixed
Improve performance in search pages.

## v0.1.2 - 25.4.2024
### Fixed
* Bug with invitation in RPOs.

### Removed
* Remove number of products in researcher dashboard page.

## v0.1.1 - 18.4.2024
### Fixed
* Bug with router links to search pages in all entities.

### Changed
* Filters replace result with publication, dataset, software, other.
* Range Filter override initial values of a chart.

### Removed
* Remove search bar from menu in landings.

## v0.1.0 - 17.4.2024
### Added
* Introduce copy/reference instance type in stakeholders profiles.
* Add class help texts for Engagement and training
* Add Managers in admin
* Multi indicator paths in admin and monitor page.

### Changed
* Rename existed managers to Primary Dashboard Managers
* Convert all profiles to **reference**
* Reordering in default will be inherited to children. (copy)
* Export/import Indicators add stakeholder index variables and type in order to import into another profile.
* Remove edit/remove actions in indicators page when a profile is **reference**

## v0.0.7 - 23.3.2024
### Added
* Add licence facet in results.
* Cache # of publications response
* Pop-up to every monitor with guide for the basic interactivity functions of the charts.

### Fixed
* Responsive menu items for medium screens
* Fix bug with condition if user is manager in publications method.

## v0.0.6 - 22.2.2024
### Added
* Sort by # of publications in browse pages of RFOs/RPOs.
* Override profiles in indicator level.

### Changed
* Change research queries.

### Removed
* My orcid links from beta.

### Fixed
* Bug with encoding alias.
* Link to matomo stats.

## v0.0.5 - 14.2.2024
### Added
* Country filter in all entities.

### Fixed
* Bug with encoding of index variables in url of indicators.
* Bug in section tabs.
* Bug with search to landing navigation in national monitor.

## v0.0.4 - 07.2.2024
### Added
* Home page.
* Orcid page to discover your results.
* Show filters in subtitle of a chart.
* Show selected filters in indicators.
* Sections: Add description.

### Changed
* Section changed to tabs.
* Filter OA color changed to Publisher Access.

### Fixed
* Fos filter in charts.

## v0.0.3 - 29.1.2024
### Added
* Add info floating button to all pages and link to terminology page.

### Changed
* Change OA Routes to Access Routes.
* Make contact-us full screen modal.
* Move floating message button to root components.


## v0.0.2 - 18.1.2024
### Added
* Loading in dashboard - cache for FOS call.

### Removed
* Remove imported from file in sections titles.
* Remove loading in dashboard when a subcategory is changed.

### Fixed
* Export indicators with file name.
* Invitation of manager doesn't work as expected.
* Sandbox guard for researcher.

## v0.0.1 - 10.1.2024
* Initial Release.
