# Irish Monitor

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.0.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:5500/`. The application will automatically reload if you change any of the source files.

## Sandbox environment (BETA) build
1. Run `npm run build:ssr-beta` to build the project.
2. Copy the folder to another location and navigate to it.
3. Run `npm run after-build-clean` to remove git unnecessary files in the new folder.
4. Upload to the server.
5. Run `npm install` in the uploaded folder and copy the content to the server location.
6. Restart the service.

## Production environment build
1. Run `npm run build:ssr-prod` to build the project.
2. Copy the folder to another location and navigate to it.
3. Run `npm run after-build-clean` to remove git unnecessary files in the new folder.
4. Upload to the server.
5. Run `npm install` in the uploaded folder and copy the content to the server location.
6. Restart the service.

## Notes
1. Login Session is only working under the same domain with the login-service of the [properties](src/environments/environment.ts).
2. Search service, ORCID-Integrated service, Linking service belongs to OpenAIRE.
3. Managers are stored at OpenAIRE AAI Registry.
4. Irish Monitor Service is the main service of Irish Monitor.

## License

This project is licensed under the Apache License, Version 2.0. See the [LICENSE](LICENSE.txt) file for details.

