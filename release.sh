rm -f *.zip

# Portal
git clone https://code-repo.d4science.org/MaDgIK/irish-monitor.git
cd irish-monitor
git checkout master
git submodule update --init --recursive
cd src/app/openaireLibrary
git checkout master
cd ../../assets/common-assets
git checkout master
cd ../openaire-theme
git checkout master
cd ../../../
npm run after-build-clean
version=$(npm pkg get version | xargs)
cd ../
zip -r irish-monitor-$version.zip irish-monitor
rm -rf irish-monitor

# Service
git clone https://code-repo.d4science.org/MaDgIK/irish-monitor-service.git
cd irish-monitor-service
git checkout production
rm -rf .git*
version=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
# Remove the '-SNAPSHOT' suffix
version=${version%-SNAPSHOT}
# Split the version string into major, minor, and patch components
IFS='.' read -r major minor patch <<EOF
$version
EOF
# Subtract one from the patch version
patch=$((patch - 1))
# Reconstruct the version string
version="$major.$minor.$patch"
cd ../
zip -r irish-monitor-service-$version.zip irish-monitor-service
rm -rf irish-monitor-service
