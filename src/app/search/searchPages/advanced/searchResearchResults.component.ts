import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SearchBaseComponent} from "../searchBaseComponent";
import {CustomFilterService} from "../../../openaireLibrary/shared/customFilter.service";

@Component({
  selector: 'monitor-advanced-search-results',
  template: `
      <search-research-results *ngIf="customFilters" resultType="result" [simpleView]="false"
                               [customFilters]=customFilters [hasPrefix]="false"
                               [showBreadcrumb]="false"
                               [showSwitchSearchLink]="true"
                               [searchForm]="{dark: false, class: 'search-form'}"
                               [pageTitlePrefix]="title"
      ></search-research-results>
  `
})
export class MonitorAdvancedSearchResearchResultsComponent extends SearchBaseComponent implements OnInit{
  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected customFilterService: CustomFilterService) {
    super();
  }
}
