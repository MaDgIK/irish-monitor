import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SearchBaseComponent} from "../searchBaseComponent";
import {CustomFilterService} from "../../../openaireLibrary/shared/customFilter.service";

@Component({
  selector: 'monitor-advanced-search-organizations',
  template: `
      <search-organizations *ngIf="customFilters" [simpleView]="false" [showSwitchSearchLink]="true" [customFilters]="customFilters"
                            [hasPrefix]="false" [showBreadcrumb]="false"
                            [searchForm]="{dark: false, class: 'search-form'}">
      </search-organizations>
  `
})
export class MonitorAdvancedSearchOrganizationsComponent extends SearchBaseComponent implements OnInit{
  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected customFilterService: CustomFilterService) {
    super();
  }
}

