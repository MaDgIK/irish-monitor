import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorAdvancedSearchProjectsComponent} from './searchProjects.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: MonitorAdvancedSearchProjectsComponent, canDeactivate: [PreviousRouteRecorder]  }

    ])
  ]
})
export class SearchProjectsRoutingModule { }
