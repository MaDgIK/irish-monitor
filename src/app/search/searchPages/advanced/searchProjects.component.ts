import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SearchBaseComponent} from "../searchBaseComponent";
import {CustomFilterService} from "../../../openaireLibrary/shared/customFilter.service";

@Component({
  selector: 'monitor-advanced-search-projects',
  template: `
      <search-projects *ngIf="customFilters" [simpleView]="false"
                       [customFilters]=customFilters
                       [hasPrefix]="false"
                       [showBreadcrumb]="false"
                       [showSwitchSearchLink]="true"
                       [searchForm]="{dark: false, class: 'search-form'}"
      >
      </search-projects>
  `
  
})
export class MonitorAdvancedSearchProjectsComponent  extends SearchBaseComponent implements OnInit{
  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected customFilterService: CustomFilterService) {
    super();
  }
}
