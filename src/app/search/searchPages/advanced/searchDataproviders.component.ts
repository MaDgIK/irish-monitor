import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SearchBaseComponent} from "../searchBaseComponent";
import {CustomFilterService} from "../../../openaireLibrary/shared/customFilter.service";

@Component({
  selector: 'monitor-advanced-search-dataproviders',
  template: `
      <search-dataproviders *ngIf="customFilters" [simpleView]="false"
                            [customFilters]=customFilters
                            [hasPrefix]="false"
                            [showBreadcrumb]="false"
                            [showSwitchSearchLink]="true"
                            [searchForm]="{dark: false, class: 'search-form'}">
      </search-dataproviders>
  `
})
export class MonitorAdvancedSearchDataprovidersComponent  extends SearchBaseComponent implements OnInit{
  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected customFilterService: CustomFilterService) {
    super();
  }
}
