import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SearchProjectsRoutingModule} from './searchProjects-routing.module';
import {MonitorAdvancedSearchProjectsComponent} from './searchProjects.component';
import {SearchProjectsModule} from "../../../openaireLibrary/searchPages/searchProjects.module";
import {PreviousRouteRecorder} from "../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";

@NgModule({
  imports: [
    CommonModule, FormsModule, SearchProjectsRoutingModule, SearchProjectsModule
  
  ],
  declarations: [
    MonitorAdvancedSearchProjectsComponent
  ],
  providers: [PreviousRouteRecorder],
  exports: [
    MonitorAdvancedSearchProjectsComponent
  ]
})
export class MonitorAdvancedSearchProjectsModule {
}
