import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{MonitorSearchOrganizationsComponent} from './searchOrganizations.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';



@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: MonitorSearchOrganizationsComponent, canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class SearchOrganizationsRoutingModule { }
