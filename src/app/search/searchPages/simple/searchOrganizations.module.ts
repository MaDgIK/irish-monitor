import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SearchOrganizationsRoutingModule} from './searchOrganizations-routing.module';
import {MonitorSearchOrganizationsComponent} from './searchOrganizations.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {SearchOrganizationsModule} from "../../../openaireLibrary/searchPages/searchOrganizations.module";
import {SearchInputModule} from "../../../openaireLibrary/sharedComponents/search-input/search-input.module";
import {RouterModule} from "@angular/router";


@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    SearchOrganizationsRoutingModule, SearchOrganizationsModule, SearchInputModule

  ],
  declarations: [
    MonitorSearchOrganizationsComponent
  ],
  providers: [PreviousRouteRecorder],
  exports: [
    MonitorSearchOrganizationsComponent
  ]
})
export class MonitorSearchOrganizationsModule {
}
