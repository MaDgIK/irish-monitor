import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SearchResearchResultsRoutingModule} from './searchResearchResults-routing.module';
import {MonitorSearchResearchResultsComponent} from './searchResearchResults.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {SearchResearchResultsModule} from "../../../openaireLibrary/searchPages/searchResearchResults.module";
import {SearchInputModule} from "../../../openaireLibrary/sharedComponents/search-input/search-input.module";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    SearchResearchResultsRoutingModule, SearchResearchResultsModule, SearchInputModule

  ],
  declarations: [
    MonitorSearchResearchResultsComponent
  ],
  providers: [PreviousRouteRecorder],
  exports: [
    MonitorSearchResearchResultsComponent
  ]
})
export class MonitorSearchResearchResultsModule {
}
