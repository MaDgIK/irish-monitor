import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MonitorSearchDataprovidersComponent} from './searchDataproviders.component';
import {SearchDataProvidersRoutingModule} from './searchDataProviders-routing.module';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {SearchDataProvidersModule} from '../../../openaireLibrary/searchPages/searchDataProviders.module';
import {SearchInputModule} from "../../../openaireLibrary/sharedComponents/search-input/search-input.module";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    SearchDataProvidersModule, SearchDataProvidersRoutingModule, SearchInputModule

  ],
  declarations: [
    MonitorSearchDataprovidersComponent
  ],
  providers: [PreviousRouteRecorder],
  exports: [
    MonitorSearchDataprovidersComponent
  ]
})
export class MonitorSearchDataProvidersModule {
}
