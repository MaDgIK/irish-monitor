import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SearchProjectsRoutingModule} from './searchProjects-routing.module';
import {MonitorSearchProjectsComponent} from './searchProjects.component';
import {SearchProjectsModule} from "../../../openaireLibrary/searchPages/searchProjects.module";
import {PreviousRouteRecorder} from "../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {SearchInputModule} from "../../../openaireLibrary/sharedComponents/search-input/search-input.module";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule, FormsModule, SearchProjectsRoutingModule, SearchProjectsModule, SearchInputModule, RouterModule

  ],
  declarations: [
    MonitorSearchProjectsComponent
  ],
  providers: [PreviousRouteRecorder],
  exports: [
    MonitorSearchProjectsComponent
  ]
})
export class MonitorSearchProjectsModule {
}
