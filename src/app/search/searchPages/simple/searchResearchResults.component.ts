import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CustomFilterService} from "../../../openaireLibrary/shared/customFilter.service";
import {SearchBaseComponent} from "../searchBaseComponent";
import {OpenaireEntities} from "../../../openaireLibrary/utils/properties/searchFields";

@Component({
  selector: 'monitor-search-results',
  template: `
    <div class="uk-flex uk-flex-center uk-margin-medium-bottom uk-margin-medium-top">
      <div class="uk-width-1-1 uk-width-auto@s uk-padding uk-padding-remove-vertical">
        <div class=" uk-flex uk-flex-right">
          <a [routerLink]="properties.searchLinkToAdvancedResults" [queryParams]="parameters"
            class="uk-margin-xsmall-bottom uk-margin-xsmall-right">Advanced search</a>
        </div>
        <div search-input [(value)]="keyword" [placeholder]="'Search by title, author, abstract, DOI, orcid...'"
           [searchInputClass]="'flat'" [iconPosition]="'left'" (searchEmitter)="search()" class="uk-width-xlarge@l uk-width-large@m uk-width-1-1"></div>
      </div>
    </div>
    <search-research-results *ngIf="customFilters" [customFilters]="customFilters" resultType="result"
                             [hasPrefix]="false" [pageTitlePrefix]="title"
                             [includeOnlyResultsAndFilter]="true" [showBreadcrumb]="false"
                             [showSwitchSearchLink]="false"
                             [searchForm]="{dark: false, class: 'search-form'}"></search-research-results>
  `,
})
export class MonitorSearchResearchResultsComponent extends SearchBaseComponent {
  openaireEntities = OpenaireEntities;

  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected customFilterService: CustomFilterService) {
    super();
  }
}
