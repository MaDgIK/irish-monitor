import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SearchBaseComponent} from "../searchBaseComponent";
import {CustomFilterService} from "../../../openaireLibrary/shared/customFilter.service";

@Component({
  selector: 'monitor-search-organizations',
  template: `
      <div class="uk-flex uk-flex-center uk-margin-medium-bottom uk-margin-medium-top">
          <div class="uk-width-1-1 uk-width-auto@s uk-padding uk-padding-remove-vertical">
              <div class=" uk-flex uk-flex-right">
                  <a [routerLink]="properties.searchLinkToAdvancedOrganizations" [queryParams]="parameters">Advanced
                      search</a>
              </div>
              <div search-input [(value)]="keyword" [placeholder]="'Search by organization name...'"
                   [searchInputClass]="'inner background'" (searchEmitter)="search()"
                   class="uk-width-xlarge@l uk-width-large@m uk-width-1-1"></div>
          </div>
      </div>
      <search-organizations *ngIf="customFilters" [customFilters]="customFilters"
                            [hasPrefix]="false"
                            [includeOnlyResultsAndFilter]="true" [showBreadcrumb]="false"
                            [showSwitchSearchLink]="false"
                            [searchForm]="{dark: false, class: 'search-form'}">
      </search-organizations>
  `
})
export class MonitorSearchOrganizationsComponent extends SearchBaseComponent{

  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected customFilterService: CustomFilterService) {
    super();
  }
}
