import { NgModule}            from '@angular/core';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import { MonitorPublicationComponent } from './publication.component';
import {PublicationRoutingModule} from './publication-routing.module';
import {ResultLandingModule} from "../../../openaireLibrary/landingPages/result/resultLanding.module";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [PublicationRoutingModule, ResultLandingModule, CommonModule],
  declarations:[MonitorPublicationComponent],
  providers:[PreviousRouteRecorder],
  exports:[MonitorPublicationComponent]
})
export class LibPublicationModule { }
