import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorPublicationComponent} from './publication.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
 imports: [
  RouterModule.forChild([
    { path: '', component: MonitorPublicationComponent, canDeactivate: [PreviousRouteRecorder]  }
  ])
]
})
export class PublicationRoutingModule { }
