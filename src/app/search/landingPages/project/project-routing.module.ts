import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorProjectComponent} from './project.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
 imports: [
  RouterModule.forChild([
    { path: '', component: MonitorProjectComponent, canDeactivate: [PreviousRouteRecorder]  }
  ])
 ]
})
export class ProjectRoutingModule { }
