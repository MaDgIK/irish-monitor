import {NgModule} from '@angular/core';
import {ProjectModule} from '../../../openaireLibrary/landingPages/project/project.module';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {MonitorProjectComponent} from './project.component';
import {ProjectRoutingModule} from './project-routing.module';
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [ProjectModule, ProjectRoutingModule, CommonModule],
  declarations:[MonitorProjectComponent],
  providers:[PreviousRouteRecorder],
  exports:[MonitorProjectComponent]
})
export class LibProjectModule { }
