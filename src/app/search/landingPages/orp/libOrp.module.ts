import {NgModule}                    from '@angular/core';
import {PreviousRouteRecorder}       from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {MonitorOrpComponent}        from './orp.component';
import {OrpRoutingModule}            from './orp-routing.module';
import {ResultLandingModule} from "../../../openaireLibrary/landingPages/result/resultLanding.module";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [
    OrpRoutingModule, ResultLandingModule, CommonModule
  ],
  declarations: [
    MonitorOrpComponent
  ],
  providers: [
     PreviousRouteRecorder
  ],
  exports: [
    MonitorOrpComponent
  ]
})
export class LibOrpModule { }
