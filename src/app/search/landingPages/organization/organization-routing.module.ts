import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorOrganizationComponent} from './organization.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
  imports: [
    RouterModule.forChild([
      {path: '', component: MonitorOrganizationComponent, canDeactivate: [PreviousRouteRecorder]}
    ])
  ]
})
export class OrganizationRoutingModule {
}
