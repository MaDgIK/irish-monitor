import {NgModule} from '@angular/core';
import {OrganizationModule} from '../../../openaireLibrary/landingPages/organization/organization.module';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {MonitorOrganizationComponent} from './organization.component';
import {OrganizationRoutingModule} from './organization-routing.module';
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [OrganizationModule, OrganizationRoutingModule, CommonModule],
  declarations:[MonitorOrganizationComponent],
  providers:[ PreviousRouteRecorder],
  exports:[MonitorOrganizationComponent]
})
export class LibOrganizationModule { }
