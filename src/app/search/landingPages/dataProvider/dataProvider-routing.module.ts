import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorDataProviderComponent} from './dataProvider.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: '', component: MonitorDataProviderComponent, canDeactivate: [PreviousRouteRecorder]}
    ])
  ]
})
export class DataProviderRoutingModule {
}
