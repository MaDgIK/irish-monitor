import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {StakeholderService} from "../../../openaireLibrary/monitor/services/stakeholder.service";
import {Subscriber} from "rxjs";

@Component({
  selector: 'monitor-dataprovider',
  template: `
    <div id="page_content">
      <dataprovider *ngIf="initialized" [communityId]="communityId"></dataprovider>
    </div>`,
})
export class MonitorDataProviderComponent {
  initialized: boolean = false;
  communityId;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private stakeholderService: StakeholderService) {
  }
  subscriptions = [];
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  ngOnInit() {
   /* this.subscriptions.push(this.route.params.subscribe(params => {
      if (params['stakeholder']) {
        this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
          if (stakeholder) {
            this.initialized = true;
            this.communityId = stakeholder.alias;
          }
        }));
      }
    }));*/
    this.initialized =true;
  }
}
