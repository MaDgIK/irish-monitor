import {NgModule} from '@angular/core';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {MonitorResultComponent} from './result.component';
import {ResultRoutingModule} from './result-routing.module';
import {ResultLandingModule} from "../../../openaireLibrary/landingPages/result/resultLanding.module";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [ResultRoutingModule, ResultLandingModule, CommonModule],
  declarations:[MonitorResultComponent],
  providers:[PreviousRouteRecorder],
  exports:[MonitorResultComponent]
})
export class LibResultModule { }
