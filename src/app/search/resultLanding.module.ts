import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    // Search Pages
    { path: '', redirectTo: 'find/research-outcomes', pathMatch: 'full'},
    { path: 'find/research-outcomes', loadChildren:  () => import('./searchPages/simple/searchResearchResults.module').then(m => m.MonitorSearchResearchResultsModule)},
    { path: 'advanced/research-outcomes', loadChildren:  () => import('./searchPages/advanced/searchResearchResults.module').then(m => m.MonitorAdvancedSearchResearchResultsModule)},
    // Landing Pages
    { path: 'result', loadChildren: () => import('./landingPages/result/libResult.module').then(m => m.LibResultModule), data: {hasMenuSearchBar: true}},
    { path: 'publication', loadChildren: () => import('./landingPages/publication/libPublication.module').then(m => m.LibPublicationModule), data: {hasMenuSearchBar: true}},
    { path: 'dataset', loadChildren: () => import('./landingPages/dataset/libDataset.module').then(m => m.LibDatasetModule), data: {hasMenuSearchBar: true}},
    { path: 'software', loadChildren: () => import('./landingPages/software/libSoftware.module').then(m => m.LibSoftwareModule), data: {hasMenuSearchBar: true}},
    { path: 'other', loadChildren: () => import('./landingPages/orp/libOrp.module').then(m => m.LibOrpModule), data: {hasMenuSearchBar: true}},
  ])]
})
export class ResultLandingModule {}

