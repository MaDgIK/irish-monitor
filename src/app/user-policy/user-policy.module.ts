import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserPolicyComponent} from "./user-policy.component";
import {LogServiceModule} from "../openaireLibrary/utils/log/LogService.module";
import {Route, RouterModule} from "@angular/router";

const routes: Route[] = [
  {
    path: '', component: UserPolicyComponent
  }
];


@NgModule({
  declarations: [UserPolicyComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes),  LogServiceModule, RouterModule
  ],
  exports: [UserPolicyComponent]
})
export class UserPolicyModule { }
