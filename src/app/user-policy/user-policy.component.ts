import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '../openaireLibrary/sharedComponents/base/base.component';
import {ActivatedRoute, Router} from '@angular/router';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {Meta, Title} from '@angular/platform-browser';
import {LogService} from "../openaireLibrary/utils/log/log.service";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {UserProfileService} from "../openaireLibrary/services/userProfile.service";

@Component({
  selector: 'user-policy',
  template: `
<div *ngIf="!loading" class="uk-container uk-container-large uk-margin-top">
  <h3>User Policy</h3>
    <div class="">
      <div>
      National Open Access Monitor - Ireland requires users to accept user privacy policy, to proceed with certain actions.<br>
       Please read the <a href="/assets/privacy-policy.pdf" target="_blank">user privacy policy</a>.
      </div>
      <div>
      <input   type="checkbox"    (change)="value = !value"> Accept policy</div> 
      <button class="uk-button uk-button-primary uk-margin-top" [class.uk-disabled]="!value" (click)="accept()"> Accept and proceed</button>
      
    </div>
  
</div>
    
  `
})
export class UserPolicyComponent extends BaseComponent implements OnInit {
  redirectUrl = null;
  hasConsent = false;
  value = false;
  redirectEnabled =true;
  loading = true;
  constructor(protected _router: Router,
              protected _route: ActivatedRoute,
              protected seoService: SEOService,
              protected _piwikService: PiwikService,
              protected _title: Title,
              protected _meta: Meta,
              private _logService:LogService,
              private userManagementsService: UserManagementService, private _userProfileService: UserProfileService) {
    super();
  }

  ngOnInit() {
    this.loading = true;
    this.title = 'User policy';
    this.description = 'OA Monitor Ireland - User policy';
    this.setMetadata();
    this.subscriptions.push(this._route.queryParams.subscribe(params => {
      this.redirectUrl = params['redirectUrl'];
      this.redirectEnabled = params['redirect'] == 'false'?false:true;
    }));
    this.subscriptions.push(this._userProfileService.getUserProfile().subscribe(userProfile => {
      this.hasConsent = userProfile.consent;
      if(this.hasConsent && this.redirectEnabled){
        this.redirect();
      }
      this.loading = false;
    }, error => {
      this.hasConsent = false;
      this.loading = false;
    }));
  }
  accept(){
    this.subscriptions.push(this._userProfileService.saveConsentInUserProfile(this.properties).subscribe(userProfile => {
      this._userProfileService.setUserProfile(userProfile);
      this.redirect();
    }));
  }
  undo(){
    this.subscriptions.push(this._userProfileService.undoConsentInUserProfile(this.properties).subscribe(userProfile => {
      this._userProfileService.setUserProfile(userProfile);
    }));
  }
  redirect() {
    //if parameters are not read yet, force them to use the function parameter
    if (this.redirectUrl && this.redirectUrl != "") {
      this.redirectUrl = decodeURIComponent(this.redirectUrl);
      this.userManagementsService.setRedirectUrl(this.redirectUrl);
      this._router.navigate(['/reload']);
    }else{
      this._router.navigate(['/reload']);
    }
  }

}
