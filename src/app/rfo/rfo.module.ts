import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {Route, RouterModule} from "@angular/router";
import {RfoComponent} from "./rfo.component";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {RoleVerificationModule} from "../openaireLibrary/role-verification/role-verification.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {LogoUrlPipeModule} from "../openaireLibrary/utils/pipes/logoUrlPipe.module";
import {IconsService} from "../openaireLibrary/utils/icons/icons.service";
import {open_access} from "../openaireLibrary/utils/icons/icons";
import {SandboxGuard} from "../shared/sandbox.guard";
import {OaIndicatorModule} from "../shared/oa-indicator/oa-indicator.module";

const routes: Route[] = [
  {
    path: '', component: RfoComponent, children: [
      {path: '', loadChildren: () => import('../shared/browse-stakeholders/browse-stakeholders.module').then(m => m.BrowseStakeholdersModule)},
      {path: ':stakeholder/search', loadChildren: () => import('../search/resultLanding.module').then(m => m.ResultLandingModule), canActivateChild: [SandboxGuard]},
      {path: ':stakeholder', loadChildren: () => import('../shared/monitor/monitor.module').then(m => m.MonitorModule), canActivateChild: [SandboxGuard]}
    ]
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), LoadingModule, RoleVerificationModule, IconsModule, LogoUrlPipeModule, OaIndicatorModule],
  declarations: [RfoComponent],
  exports: [RfoComponent],
})
export class RfoModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([open_access]);
  }
}
