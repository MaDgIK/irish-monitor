import {Component, OnInit} from "@angular/core";
import {Stakeholder} from "../openaireLibrary/monitor/entities/stakeholder";
import {StakeholderService} from "../openaireLibrary/monitor/services/stakeholder.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {Meta, Title} from "@angular/platform-browser";
import {SearchCustomFilter} from "../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {LinksResolver} from "../search/links-resolver";
import {CustomFilterService} from "../openaireLibrary/shared/customFilter.service";
import {StakeholderBaseComponent} from "../openaireLibrary/monitor-admin/utils/stakeholder-base.component";
import {LayoutService} from "../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {OpenaireEntities} from "../openaireLibrary/utils/properties/searchFields";
import {ConfigurationService} from "../openaireLibrary/utils/configuration/configuration.service";
import {Irish} from "../shared/irish";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {Session} from "../openaireLibrary/login/utils/helper.class";
import {properties as beta} from "../../environments/environment.beta";

@Component({
  selector: 'rfo',
  template: `
      <loading *ngIf="loading" class="uk-position-center"></loading>
      <div *ngIf="!loading">
          <div class="uk-banner uk-light">
              <div class="uk-container uk-container-large">
                  <div class="uk-padding-small uk-padding-remove-vertical">
                      <div *ngIf="stakeholder" class="uk-grid uk-grid-large uk-flex-middle uk-margin-medium-bottom"
                           uk-grid>
                          <div class="uk-width-expand">
                              <div class="uk-grid uk-grid-small uk-flex-middle" uk-grid>
                                  <div class="uk-card uk-card-default uk-padding-small">
                                      <img *ngIf="stakeholder.logoUrl; else elseBlock" [src]="stakeholder | logoUrl"
                                           [alt]="stakeholder.name + ' logo'" class="uk-height-max-xsmall"
                                           style="max-width: 180px;">
                                      <ng-template #elseBlock>
                                          <img src="assets/common-assets/placeholder.png"
                                               alt="OpenAIRE placeholder logo" class="uk-height-max-xsmall"
                                               style="max-width: 180px;">
                                      </ng-template>
                                  </div>
                                  <div class="uk-width-expand uk-margin-small-left">
                                      <div class="uk-text-small uk-flex uk-flex-middle uk-margin-small-bottom">
                                          <icon [name]="'navigate_before'" [flex]="true" [ratio]="0.8"
                                                class="uk-margin-xsmall-right"></icon>
                                          <a routerLink=".">Back to
                                              all {{ entities.funder }} {{ entities.stakeholders }}</a>
                                      </div>
                                      <h1 class="uk-h4 uk-margin-remove uk-text-truncate">{{ stakeholder.name }}</h1>
                                  </div>
                              </div>
                          </div>
                          <div class="uk-width-auto">
                              <oa-indicator [stakeholder]="stakeholder"></oa-indicator>
                          </div>
                      </div>
                      <h1 *ngIf="!stakeholder" class="uk-h3 uk-margin-small-bottom">Research Funding Organisations
                          ({{ entities.funders }})</h1>
                  </div>
              </div>
              <div *ngIf="stakeholder" class="uk-banner-footer">
                  <div class="uk-container uk-container-large uk-flex uk-flex-between">
                      <ul class="uk-banner-tab uk-padding-small uk-padding-remove-vertical">
                          <li [class.uk-active]="!isSearch">
                              <a [routerLink]="['./', stakeholder.alias]" [relativeTo]="_route"
                                 (click)="isSearch = false">{{ entities.stakeholder }}</a>
                          </li>
                          <li [class.uk-active]="isSearch">
                              <a [routerLink]="['./', stakeholder.alias, 'search']"
                                 [queryParams]="routerHelper.createQueryParams(['type','peerreviewed'], [quote('publications'), quote('true')])"
                                 [relativeTo]="_route">Browse {{ openaireEntities.RESULTS }}</a>
                          </li>
                      </ul>
                      <div *ngIf="!isMobile" class="uk-margin-large-right uk-flex uk-flex-middle">
                          <a *ngIf="isManager" [routerLink]="adminLink" target="_blank"
                             class="uk-button uk-flex uk-flex-middle uk-margin-small-right">
                              Manage
                          </a>
                          <a *ngIf="sandboxLink" [href]="sandboxLink" target="_blank"
                             class="uk-button uk-flex uk-flex-middle uk-margin-small-right">
                              Sandbox
                          </a>
                      </div>
                  </div>
              </div>
          </div>
          <div *ngIf="!this.alias || this.stakeholder">
              <router-outlet></router-outlet>
          </div>
      </div>
      <role-verification *ngIf="stakeholder" [id]="stakeholder.alias" [name]="stakeholder.name"
                         [type]="stakeholder.type" [service]="'irish'"
                         [relativeTo]="null" [dashboard]="'National Open Access Monitor, Ireland'"></role-verification>
  `
})
export class RfoComponent extends StakeholderBaseComponent implements OnInit {
  stakeholder: Stakeholder;
  alias: string;
  isSearch: boolean = false;
  isMobile: boolean = false;
  loading: boolean = false;
  openaireEntities = OpenaireEntities;

  constructor(private stakeholderService: StakeholderService,
              private userManagementService: UserManagementService,
              private layoutService: LayoutService,
              private _customFilterService: CustomFilterService,
              protected _router: Router,
              protected _route: ActivatedRoute,
              protected seoService: SEOService,
              protected _piwikService: PiwikService,
              protected _title: Title,
              protected _meta: Meta,
              private configurationService: ConfigurationService) {
    super();
    super.initRouterParams(this._route, event => {
      this.isSearch = event.url.includes('/search');
    });

  }

  ngOnInit() {
    this.layoutService.setRootClass('funder');
    this.title = 'RFOs';
    this.description = 'RFOs';
    this.setMetadata();
    this.subscriptions.push(this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;
    }));
    this.params.subscribe(params => {
      this.alias = params['stakeholder'];
      if (this.alias) {
        if(this.stakeholder?.alias !== this.alias) {
          this.loading = true;
          this._customFilterService.setCustomFilter(null);
          this.subscriptions.push(this.stakeholderService.getStakeholder(this.alias, true).subscribe(stakeholder => {
            this.stakeholder = stakeholder;
            if (this.stakeholder && this.stakeholder.type === 'funder') {
              this.setProperties(this.stakeholder.alias, this.stakeholder.type, this.configurationService);
              LinksResolver.resetProperties();
              LinksResolver.setSearchAndResultLanding("rfo/" + this.stakeholder.alias);
              let value = this.stakeholder.index_id + "||" + this.stakeholder.index_name + "||" + this.stakeholder.index_shortName;
              this._customFilterService.setCustomFilter(
                  [new SearchCustomFilter("National", "country", "IE", "Irish National Monitor", false),
                    new SearchCustomFilter("RF0", "relfunder", value, this.stakeholder.index_name, true)
                  ]);
              this.loading = false;
            } else {
              this.navigateToError();
            }
          }));
        }
      } else {
        this.stakeholder = null;
        this._customFilterService.setCustomFilter(null);
      }
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.layoutService.setRootClass(null);
    this.setProperties(Irish.irishAdminToolsCommunity, Irish.irishAdminToolsPortalType, this.configurationService);
    LinksResolver.resetProperties();
  }

  get isManager() {
    return Session.isPortalAdministrator(this.userManagementService.user) ||
        Session.isCurator(this.stakeholder.type, this.userManagementService.user) ||
        Session.isManager(this.stakeholder.type, this.stakeholder.alias, this.userManagementService.user);
  }

  get isMember() {
    return this.isManager ||
        Session.isMember(this.stakeholder.type, this.stakeholder.alias, this.userManagementService.user);
  }

  get adminLink() {
    return "/admin/" + this.stakeholder.alias;
  }

  get sandboxLink() {
    if(this.properties.environment !== 'beta' && this.isMember) {
      return beta.domain + '/rfo/' + this.stakeholder.alias;
    } else {
      return null;
    }
  }
}
