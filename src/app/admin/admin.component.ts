import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {StakeholderService} from "../openaireLibrary/monitor/services/stakeholder.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SidebarBaseComponent} from "../openaireLibrary/dashboard/sharedComponents/sidebar/sidebar-base.component";
import {LayoutService} from "../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {Stakeholder} from "../openaireLibrary/monitor/entities/stakeholder";
import {MenuItem} from "../openaireLibrary/sharedComponents/menu";
import {Session, User} from "../openaireLibrary/login/utils/helper.class";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {ConnectHelper} from "../openaireLibrary/connect/connectHelper";
import {ConfigurationService} from "../openaireLibrary/utils/configuration/configuration.service";
import {Irish} from "../shared/irish";

@Component({
  selector: 'admin',
  template: `
      <div>
          <loading *ngIf="loading" class="uk-position-center"></loading>
          <div *ngIf="!loading" class="sidebar_main_swipe uk-flex uk-background-default"
               [class.sidebar_main_active]="open && (hasSidebar || hasInternalSidebar || hasAdminMenu)"
               [class.sidebar_mini]="!open && (hasSidebar || hasInternalSidebar || hasAdminMenu)"
               [class.sidebar_hover]="hover">
              <dashboard-sidebar *ngIf="hasSidebar && !hasInternalSidebar && sideBarItems.length > 0"
                                 [logoURL]="stakeholder | logoUrl"
                                 [items]="sideBarItems" [backItem]="backItem"></dashboard-sidebar>
              <dashboard-sidebar *ngIf="hasAdminMenu && !hasInternalSidebar" [items]="adminMenuItems"
                                 [backItem]="backItem"></dashboard-sidebar>
              <div class="uk-width-1-1">
                  <router-outlet></router-outlet>
              </div>
          </div>
          <cache-indicators *ngIf="stakeholder && isAdmin" [alias]="stakeholder.alias"></cache-indicators>
      </div>
  `
})
export class AdminComponent extends SidebarBaseComponent implements OnInit {
  stakeholder: Stakeholder;
  loading: boolean = true;
  user: User;

  constructor(protected _route: ActivatedRoute,
              protected _router: Router,
              protected layoutService: LayoutService,
              protected cdr: ChangeDetectorRef,
              private stakeholderService: StakeholderService,
              private userManagementService: UserManagementService,
              private configurationService: ConfigurationService) {
    super();
    this.initRouterParams(this._route);
  }

  ngOnInit() {
    super.ngOnInit();
    this.layoutService.setHasHelpPopUp(false);
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
      this.subscriptions.push(this.params.subscribe(params => {
        this.sideBarItems = [];
        this.loading = true;
        if(params['stakeholder']) {
          this.subscriptions.push(this.stakeholderService.getStakeholder(params['stakeholder']).subscribe(stakeholder => {
            this.stakeholder = stakeholder;
            this.setProperties(this.stakeholder.alias, this.stakeholder.type);
            this.layoutService.setRootClass(this.stakeholder.type);
            this.sideBarItems = [];
            this.sideBarItems.push(new MenuItem("general", "General", "", "/admin/" + this.stakeholder.alias, false, [], [], {}, {name: 'badge'}));
            this.sideBarItems.push(new MenuItem("indicators", "Indicators", "", "/admin/" + this.stakeholder.alias + '/indicators', false, [], [], {}, {name: 'bar_chart'}, null, "uk-visible@m"));
            if (this.stakeholder.defaultId) {
              this.sideBarItems.push(new MenuItem("users", "Users", "", "/admin/" + this.stakeholder.alias + '/users', false, [], [], {}, {name: 'group'}, null, "uk-visible@m", "/admin/" + this.stakeholder.alias + "/users"));
            }
            if (this.isAdmin && this.stakeholder.defaultId) {
              this.sideBarItems.push(new MenuItem("admin-tools", "Pages & Entities", "", "/admin/" + this.stakeholder.alias + "/admin-tools/pages", false, [], [], {}, {name: 'description'}, null, "uk-visible@m", "/admin/" + this.stakeholder.alias + "/admin-tools"));
            }
            this.backItem = null;
            if (this.hasAdminMenu) {
              this.setAdminMenuItems();
            }
            this.backItem = new MenuItem("back", "Manage profiles", "", "/admin", false, [], null, {}, {name: 'west'});
            this.loading = false;
          }));
        } else {
          this.setProperties(Irish.irishAdminToolsCommunity, Irish.irishAdminToolsPortalType);
          this.stakeholderService.setStakeholder(null);
          this.layoutService.setRootClass(null);
          this.hasSidebar = false;
          this.loading = false;
          if (this.hasAdminMenu) {
            this.setAdminMenuItems();
          }
        }
      }));
    }));
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.layoutService.setHasHelpPopUp(true);
    this.layoutService.setRootClass(null);
    this.setProperties(Irish.irishAdminToolsCommunity, Irish.irishAdminToolsPortalType);
  }

  setProperties(id, type = null) {
    this.properties.adminToolsCommunity = id;
    if (type) {
      this.properties.adminToolsPortalType = type;
    } else {
      ConnectHelper.setPortalTypeFromPid(id);
    }
    this.configurationService.initPortal(this.properties, this.properties.adminToolsCommunity);
  }

  private setAdminMenuItems() {
    this.adminMenuItems = [];
    this.adminMenuItems.push(new MenuItem("stakeholders", "Manage profiles", "", "/admin", false, [], [], {}, {name: 'settings'}));
    if (Session.isPortalAdministrator(this.user)) {
      this.adminMenuItems.push(new MenuItem("adminOptions", "Super Admin options", "", "/admin/admin-tools/portals", false, [], [], {}, {name: 'settings'}, null, "uk-visible@m", '/admin/admin-tools'));
    }
    if (Session.isPortalAdministrator(this.user) || Session.isMonitorCurator(this.user)) {
      this.adminMenuItems.push(new MenuItem("irishOptions", "Irish options", "", "/admin/irish/admin-tools/pages", false, [], [], {}, {name: 'settings'}, null, "uk-visible@m", '/admin/irish/admin-tools'));
    }
    this.hasAdminMenu = this.hasAdminMenu && this.adminMenuItems.length > 1;
  }

  public get isAdmin() {
    return Session.isPortalAdministrator(this.user) || Session.isCurator(this.stakeholder.type, this.user);
  }
}
