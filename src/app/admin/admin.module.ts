import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {AdminComponent} from "./admin.component";
import {HelperFunctions} from "../openaireLibrary/utils/HelperFunctions.class";
import {SideBarModule} from "../openaireLibrary/dashboard/sharedComponents/sidebar/sideBar.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {LogoUrlPipeModule} from "../openaireLibrary/utils/pipes/logoUrlPipe.module";
import {AdminLoginGuard} from "../openaireLibrary/login/adminLoginGuard.guard";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";
import {AdminDashboardGuard} from "../openaireLibrary/monitor-admin/utils/adminDashboard.guard";
import {CacheIndicatorsModule} from "../openaireLibrary/monitor-admin/utils/cache-indicators/cache-indicators.module";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '', component: AdminComponent, children: [
        {
          path: '',
          canActivateChild: [LoginGuard],
          loadChildren: () => import('../openaireLibrary/monitor-admin/manageStakeholders/manageStakeholders.module').then(m => m.ManageStakeholdersModule),
          data: {hasAdminMenu: true, hasSidebar: false}
        },
        {
          path: 'admin-tools',
          loadChildren: () => import('../admin-tools/portal-admin-tools-routing.module').then(m => m.PortalAdminToolsRoutingModule),
          canActivateChild: [AdminLoginGuard],
          data: {hasAdminMenu: true, hasSidebar: false}
        },
        {
          path: ':stakeholder',
          canActivateChild: [AdminDashboardGuard],
          children: [
            {
              path: '',
              loadChildren: () => import('../openaireLibrary/monitor-admin/general/general.module').then(m => m.GeneralModule)
            },
            {
              matcher: HelperFunctions.routingMatcher(['indicators', 'indicators/:topic']),
              loadChildren: () => import('../openaireLibrary/monitor-admin/topic/topic.module').then(m => m.TopicModule),
              data: {hasInternalSidebar: true, showLogo: true},
              pathMatch: 'full'
            },
            {
              path: 'users',
              redirectTo: 'users/manager',
              pathMatch: 'full'
            },
            {
              path: 'users/:user_type',
              loadChildren: () => import('./users/users.module').then(m => m.UsersModule),
              pathMatch: 'full'
            },
            {
              path: 'admin-tools',
              loadChildren: () => import('../admin-tools/admin-tools-routing.module').then(m => m.AdminToolsRoutingModule),
              data: {param: 'stakeholder', parentClass: 'monitor'}
            }
          ]
        }
      ]
    }
  ]), SideBarModule, LoadingModule, LogoUrlPipeModule, CacheIndicatorsModule],
  declarations: [AdminComponent],
  exports: [AdminComponent]
})
export class AdminModule {

}
