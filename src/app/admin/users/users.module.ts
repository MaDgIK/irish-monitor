import {NgModule} from "@angular/core";
import {UsersComponent} from "./users.component";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {LoadingModule} from "../../openaireLibrary/utils/loading/loading.module";
import {PageContentModule} from "../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {RoleUsersModule} from "../../openaireLibrary/dashboard/users/role-users/role-users.module";
import {LogoUrlPipeModule} from "../../openaireLibrary/utils/pipes/logoUrlPipe.module";

@NgModule({
  imports: [CommonModule, LoadingModule, PageContentModule, RoleUsersModule, RouterModule, LogoUrlPipeModule, RouterModule.forChild([
    {
      path: '',
      component: UsersComponent,
      canDeactivate: [PreviousRouteRecorder]
    }
  ])],
  declarations: [UsersComponent],
  exports: [UsersComponent]
})
export class UsersModule {
}
