import {Component} from "@angular/core";
import {StakeholderBaseComponent} from "../../openaireLibrary/monitor-admin/utils/stakeholder-base.component";
import {Stakeholder} from "../../openaireLibrary/monitor/entities/stakeholder";
import {Role, RoleUtils, User} from "../../openaireLibrary/login/utils/helper.class";
import {Email} from "../../openaireLibrary/utils/email/email";
import {Composer} from "../../openaireLibrary/utils/email/composer";
import {StakeholderService} from "../../openaireLibrary/monitor/services/stakeholder.service";
import {UserManagementService} from "../../openaireLibrary/services/user-management.service";
import {ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {StringUtils} from "../../openaireLibrary/utils/string-utils.class";
import {ConfigurationService} from "../../openaireLibrary/utils/configuration/configuration.service";
import {Irish} from "../../shared/irish";

type Tab = 'member' | 'manager';

@Component({
  selector: 'users',
  templateUrl: 'users.component.html'
})
export class UsersComponent extends StakeholderBaseComponent {
	public stakeholder: Stakeholder;
  public roleUtils: RoleUtils = new RoleUtils();
  public link: string;
  public loading: boolean;
  public messages: Map<Tab, string> = new Map<Tab, string>();
  public tab: Tab = 'manager';
  public user: User;
  public emailComposer: Function = (name, recipient, role): Email => {
    return Composer.composeMessageForIrishDashboard(name, recipient, role);
  }

  constructor(private stakeholderService: StakeholderService,
              private userManagementService: UserManagementService,
              protected _route: ActivatedRoute,
              protected _title: Title,
              private configurationService: ConfigurationService) {
    super();
  }
  
  ngOnInit() {
    this.loading = true;
    this.subscriptions.push(this._route.params.subscribe(params => {
      if (this.isTab(params['user_type'])) {
        this.tab = params['user_type'];
      } else {
        this.tab = 'manager';
      }
      if(this.stakeholder) {
        this.title = this.stakeholder.name + " | " + this.users;
        this.setMetadata();
      }
    }));
    this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
      if (stakeholder) {
				this.stakeholder = stakeholder;
        this.setProperties(this.stakeholder.alias, this.stakeholder.type, this.configurationService);
        this.title = this.stakeholder.name + " | " + this.users;
        this.setMetadata();
        this.link = this.getURL(this.stakeholder.alias);
        this.messages.set("member", 'A '  + this.roleUtils.roles.member + ' has the right to access the <b>sandbox</b> of this indicator\'s profile. ' +
          'A ' + this.roleUtils.roles.member + ' has <b>no access</b> to the administration part of the profile.');
        this.messages.set("manager", 'A '  + this.roleUtils.roles.manager + ' has the right to access the <b>administration part</b> of this indicator\'s profile, ' +
          'where he is able to invite other users as '  + this.roleUtils.roles.member + 's and access the <b>sandbox.</b>');
        this.loading = false;
      }
    }));
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
    }));
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.setProperties(Irish.irishAdminToolsCommunity, Irish.irishAdminToolsPortalType, this.configurationService);
  }

  get users(): string {
    return StringUtils.capitalize(this.tab) + 's';
  }
  
  private isTab(tab: Tab): boolean {
   switch (tab) {
     case "manager":
       return true;
     case "member":
       return true;
     default:
       return false;
   }
  }
  
  private getURL(id: string): string {
    let url = this.properties.domain + this.properties.baseLink;
    if(this.stakeholder.type === 'funder') {
      url = url + '/rfo/' + id;
    } else if(this.stakeholder.type === 'organization') {
      url = url + '/rpo/' + id;
    } else if(this.stakeholder.type === 'datasource') {
      url = url + '/repository/' + id;
    } else if(this.stakeholder.type === 'country') {
      url = url + '/national';
    }
    return  url + "?verify=";
  }
}
