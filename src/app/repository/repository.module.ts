import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RepositoryComponent} from "./repository.component";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {Route, RouterModule} from "@angular/router";
import {RoleVerificationModule} from "../openaireLibrary/role-verification/role-verification.module";
import {SearchDataprovidersService} from "../openaireLibrary/services/searchDataproviders.service";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {EntityMetadataModule} from "../openaireLibrary/landingPages/landing-utils/entity-metadata.module";
import {IconsService} from "../openaireLibrary/utils/icons/icons.service";
import {open_access} from "../openaireLibrary/utils/icons/icons";
import {LogoUrlPipeModule} from "../openaireLibrary/utils/pipes/logoUrlPipe.module";
import {SandboxGuard} from "../shared/sandbox.guard";
import {OaIndicatorModule} from "../shared/oa-indicator/oa-indicator.module";

const routes: Route[] = [
  {
    path: '', component: RepositoryComponent, children: [
      {path: '', loadChildren: () => import('./browse-repositories/browse-repositories.module').then(m => m.BrowseRepositoriesModule)},
      {path: ':stakeholder/search', loadChildren: () => import('../search/resultLanding.module').then(m => m.ResultLandingModule), canActivateChild: [SandboxGuard]},
      {path: ':stakeholder', loadChildren: () => import('../shared/monitor/monitor.module').then(m => m.MonitorModule), canActivateChild: [SandboxGuard]}
    ]
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), LoadingModule, RoleVerificationModule, IconsModule, EntityMetadataModule, LogoUrlPipeModule, OaIndicatorModule],
  declarations: [RepositoryComponent],
  providers: [SearchDataprovidersService],
  exports: [RepositoryComponent],
})
export class RepositoryModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([open_access]);
  }
}
