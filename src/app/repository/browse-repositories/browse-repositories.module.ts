import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {BrowseRepositoriesComponent} from "./browse-repositories.component";
import {LoadingModule} from "../../openaireLibrary/utils/loading/loading.module";
import {PreviousRouteRecorder} from "../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {SearchInputModule} from "../../openaireLibrary/sharedComponents/search-input/search-input.module";
import {InputModule} from "../../openaireLibrary/sharedComponents/input/input.module";
import {PagingModule} from "../../openaireLibrary/utils/paging.module";
import {SearchDataprovidersService} from "../../openaireLibrary/services/searchDataproviders.service";
import {EntityMetadataModule} from "src/app/openaireLibrary/landingPages/landing-utils/entity-metadata.module";

@NgModule({
  imports: [CommonModule, LoadingModule, SearchInputModule, InputModule, PagingModule, EntityMetadataModule,
    RouterModule.forChild([
      {path: '', component: BrowseRepositoriesComponent, canDeactivate: [PreviousRouteRecorder]}
    ])],
  declarations: [BrowseRepositoriesComponent],
  providers: [SearchDataprovidersService],
  exports: [BrowseRepositoriesComponent]
})
export class BrowseRepositoriesModule {

}