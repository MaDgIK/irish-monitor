import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {StakeholderExtended} from "./irish";
import {properties} from "../../environments/environment";
import {Stakeholder, StakeholderType} from "../openaireLibrary/monitor/entities/stakeholder";
import {CustomOptions} from "../openaireLibrary/services/servicesUtils/customOptions.class";
import {catchError, map} from "rxjs/operators";
import {HelperFunctions} from "../openaireLibrary/utils/HelperFunctions.class";
import {StakeholderService} from "../openaireLibrary/monitor/services/stakeholder.service";

@Injectable({
  providedIn: 'root'
})
export class IrishMonitorService {


  constructor(private http: HttpClient,
              private stakeholderService: StakeholderService) {
  }

  public getStakeholder(id: string): Observable<StakeholderExtended> {
    return this.http.get<StakeholderExtended>(properties.monitorServiceAPIURL + 'extended/' + id, CustomOptions.registryOptions()).pipe(map(stakeholder => {
      return HelperFunctions.copy(Stakeholder.checkIsUpload(stakeholder));
    }));
  }

  public getStakeholders(type: StakeholderType): Observable<StakeholderExtended[]> {
    return this.http.get<StakeholderExtended[]>(properties.monitorServiceAPIURL + 'extended?type=' + type, CustomOptions.registryOptions()).pipe(catchError(err => {
      return this.stakeholderService.getStakeholders(type, null);
    }), map(stakeholders => {
      return HelperFunctions.copy(Stakeholder.checkIsUpload(stakeholders));
    }));
  }
}
