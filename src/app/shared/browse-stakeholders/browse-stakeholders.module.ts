import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BrowseStakeholdersComponent} from "./browse-stakeholders.component";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {SearchInputModule} from "../../openaireLibrary/sharedComponents/search-input/search-input.module";
import {LoadingModule} from "../../openaireLibrary/utils/loading/loading.module";
import {InputModule} from "../../openaireLibrary/sharedComponents/input/input.module";
import {PagingModule} from "../../openaireLibrary/utils/paging.module";
import {LogoUrlPipeModule} from "../../openaireLibrary/utils/pipes/logoUrlPipe.module";
import {IconsModule} from "../../openaireLibrary/utils/icons/icons.module";
import {IconsService} from "../../openaireLibrary/utils/icons/icons.service";
import {open_access} from "../../openaireLibrary/utils/icons/icons";

@NgModule({
  imports: [CommonModule, LoadingModule, SearchInputModule, InputModule, PagingModule,
    LogoUrlPipeModule, IconsModule,
    RouterModule.forChild([
      {path: '', component: BrowseStakeholdersComponent, canDeactivate: [PreviousRouteRecorder]}
    ])],
  declarations: [BrowseStakeholdersComponent],
  exports: [BrowseStakeholdersComponent]
})
export class BrowseStakeholdersModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([open_access]);
  }
}
