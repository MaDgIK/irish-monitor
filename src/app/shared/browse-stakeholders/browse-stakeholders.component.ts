import {ChangeDetectorRef, Component} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {LayoutService} from "src/app/openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {FormBuilder} from "@angular/forms";
import {IrishMonitorService} from "../irish-monitor.service";
import {StakeholderExtended} from "../irish";
import {Option} from "../../openaireLibrary/sharedComponents/input/input.component";
import {
  BrowseStakeholderBaseComponent
} from "../../openaireLibrary/monitor/browse-stakeholder/browse-stakeholder-base.component";

@Component({
  selector: 'browse-stakeholder',
  templateUrl: 'browse-stakeholders.component.html'
})
export class BrowseStakeholdersComponent extends BrowseStakeholderBaseComponent<StakeholderExtended> {
  sortOptions: Option[] = [
    {value: null, label: 'Number of Publications'},
    {value: 'openAccess', label: 'Open Access'},
    {value: 'alphAsc', label: 'Alphabetically Asc. (A-Z)'},
    {value: 'alphDsc', label: 'Alphabetically Dsc. (Z-A)'},
  ];

  constructor(protected _route: ActivatedRoute,
              protected _router: Router,
              protected irishMonitorService: IrishMonitorService,
              protected layoutService: LayoutService,
              protected cdr: ChangeDetectorRef,
              protected fb: FormBuilder) {
    super();
  }

  init() {
    this.subscriptions.push(this.irishMonitorService.getStakeholders(this.stakeholderType).subscribe(stakeholders => {
      this.stakeholders = stakeholders;
      this.filteredStakeholders = stakeholders;
      this.hasPublications = this.stakeholders.length > 0 && this.stakeholders.filter(stakeholder => stakeholder.publications > 0).length > 0;
      this.hasOpenAccess = this.stakeholders.length > 0 && this.stakeholders.filter(stakeholder => stakeholder.openAccess > 0).length > 0;
      if(!this.hasPublications) {
        this.sortOptions = this.sortOptions.filter(option => !!option.value);
        if(!this.hasOpenAccess) {
          this.sortOptions = this.sortOptions.filter(option => option.value === 'openAccess');
          this.sortBy = 'alphAsc';
        } else {
          this.sortBy = 'openAccess';
        }
      }
      this.sortByChanged();
      this.filtering(this.keywordControl.value);
    }));
  }

  sortByChanged() {
    switch(this.sortBy) {
      case 'alphAsc':
        this.stakeholders = this.stakeholders.sort((a, b) => a['name'].localeCompare(b['name']));
        break;
      case 'alphDsc':
        this.stakeholders = this.stakeholders.sort((a, b) => b['name'].localeCompare(a['name']));
        break;
      case 'openAccess':
        this.stakeholders = this.stakeholders.sort((a, b) => b.openAccess - a.openAccess);
        break;
      default:
        this.stakeholders = this.stakeholders.sort((a, b) => b.publications - a.publications);
        break;
    }
  }

  get year() {
    return (new Date().getFullYear() - 1).toString();
  }
}
