import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {MonitorComponent} from "./monitor.component";
import {PageContentModule} from "../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {SliderTabsModule} from "../../openaireLibrary/sharedComponents/tabs/slider-tabs.module";
import {NumberRoundModule} from "../../openaireLibrary/utils/pipes/number-round.module";
import {IconsModule} from "../../openaireLibrary/utils/icons/icons.module";
import {ClickModule} from "../../openaireLibrary/utils/click/click.module";
import {RangeFilterModule} from "../../openaireLibrary/utils/rangeFilter/rangeFilter.module";
import {SearchFilterModule} from "../../openaireLibrary/searchPages/searchUtils/searchFilter.module";
import {IconsService} from "../../openaireLibrary/utils/icons/icons.service";
import {filters} from "../../openaireLibrary/utils/icons/icons";
import {SliderUtilsModule} from "../../openaireLibrary/sharedComponents/slider-utils/slider-utils.module";
import {LoadingModule} from "../../openaireLibrary/utils/loading/loading.module";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {path: '', component: MonitorComponent},
    {path: ':topic/', component: MonitorComponent},
    {path: ':topic/:category', component: MonitorComponent},
    {path: ':topic/:category/:subCategory', component: MonitorComponent}
  ]), PageContentModule, SliderTabsModule, NumberRoundModule, IconsModule, ClickModule, RangeFilterModule, SearchFilterModule, SliderUtilsModule, LoadingModule],
  declarations: [MonitorComponent],
})
export class MonitorModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([ filters]);
  }
}
