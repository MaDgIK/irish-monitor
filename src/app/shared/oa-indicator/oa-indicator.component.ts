import {Component, Input, OnInit} from "@angular/core";
import {Stakeholder} from "../../openaireLibrary/monitor/entities/stakeholder";
import {StatisticsService} from "../../openaireLibrary/monitor-admin/utils/services/statistics.service";
import {IndicatorStakeholderBaseComponent} from "../../openaireLibrary/monitor-admin/utils/stakeholder-base.component";
import {zip} from "rxjs";
import {OAIndicator} from "../../openaireLibrary/monitor-admin/utils/indicator-utils";
import {IrishMonitorService} from "../irish-monitor.service";

@Component({
  selector: "oa-indicator",
  template: `
      <ng-container *ngIf="percentage > 0">
          <div class="uk-flex uk-flex-column uk-flex-middle">
              <div class="uk-progress-semicircle open-access" [attr.percentage]="percentage.toFixed(1)"
                   [style]="'--percentage:' + percentage.toFixed(1)"></div>
              <div class="uk-flex uk-flex-middle uk-margin-small-top">
                  <icon class="open-access" [name]="'open_access'" [flex]="true"></icon>
                  <span class="uk-margin-xsmall-left uk-text-small">Open Access with Licence ({{year}})</span>
              </div>
          </div>
          
      </ng-container>
  `
})
export class OaIndicatorComponent extends IndicatorStakeholderBaseComponent implements OnInit {
  @Input()
  public stakeholder: Stakeholder;
  public percentage: number = 0;

  constructor(private statisticsService: StatisticsService,
              private irishMonitorService: IrishMonitorService) {
    super();
  }

  ngOnInit(): void {
    if(this.stakeholder) {
      this.subscriptions.push(this.irishMonitorService.getStakeholder(this.stakeholder._id).subscribe(stakeholder => {
        this.percentage = stakeholder.openAccess;
        if(!this.percentage) {
          this.initPercentage();
        }
      }));
    }
  }

  public initPercentage(): void {
    let OAIndicator: OAIndicator = this.stakeholderUtils.openAccess.get(this.stakeholder.type);
    if(OAIndicator) {
      this.subscriptions.push(zip(
          this.statisticsService.getNumbers(this.indicatorUtils.getSourceType(OAIndicator.numerator.source), this.indicatorUtils.getFullUrlWithFilters(this.stakeholder, OAIndicator.numerator, null, this.year, this.year)),
          this.statisticsService.getNumbers(this.indicatorUtils.getSourceType(OAIndicator.denominator.source), this.indicatorUtils.getFullUrlWithFilters(this.stakeholder, OAIndicator.denominator, null, this.year, this.year))).subscribe(res => {
        let numerator = this.calculate(res[0], OAIndicator.numerator.jsonPath);
        let denominator = this.calculate(res[1], OAIndicator.denominator.jsonPath);
        if (denominator > 0) {
          this.percentage = (numerator * 100 / denominator);
        }
      }));
    }
  }

  get year() {
    return (new Date().getFullYear() - 1).toString();
  }

  public calculate(response: any, jsonPath: string[]) {
    let result = JSON.parse(JSON.stringify(response));
    jsonPath.forEach(path => {
      if (result) {
        result = result[path];
      }
    });
    if (typeof result === 'string' || typeof result === 'number') {
      result = Number(result);
      if (result === Number.NaN) {
        result = 0;
      }
    } else {
      result = 0;
    }
    return result;
  }
}
