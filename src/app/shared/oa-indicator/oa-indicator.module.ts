import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {OaIndicatorComponent} from "./oa-indicator.component";
import {IconsModule} from "../../openaireLibrary/utils/icons/icons.module";

@NgModule({
  declarations: [OaIndicatorComponent],
  imports: [CommonModule, IconsModule],
  exports: [OaIndicatorComponent]
})
export class OaIndicatorModule {}
