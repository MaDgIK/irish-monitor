import {NgModule} from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {CommonModule} from "@angular/common";
import {ResearcherComponent} from "./researcher.component";
import {SearchOrcidServiceModule} from "../openaireLibrary/claims/claim-utils/service/searchOrcidService.module";
import {SearchResearchResultsServiceModule} from "../openaireLibrary/services/searchResearchResultsService.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {IconsModule} from '../openaireLibrary/utils/icons/icons.module';
import {IconsService} from '../openaireLibrary/utils/icons/icons.service';
import {open_access} from '../openaireLibrary/utils/icons/icons';
import {SandboxGuard} from "../shared/sandbox.guard";
import {OaIndicatorModule} from "../shared/oa-indicator/oa-indicator.module";

const routes: Route[] = [
  {
    path: '', component: ResearcherComponent, children: [
      {path: '', loadChildren: () => import('./search-researcher/search-researcher.module').then(m => m.SearchResearcherModule)},
      {path: ':stakeholder/search', loadChildren: () => import('../search/resultLanding.module').then(m => m.ResultLandingModule)},
      {path: ':stakeholder', loadChildren: () => import('../shared/monitor/monitor.module').then(m => m.MonitorModule), canActivateChild: [SandboxGuard],
      data: {"researcher": true}}
    ]
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SearchOrcidServiceModule, SearchResearchResultsServiceModule, LoadingModule, IconsModule, OaIndicatorModule],
  declarations: [ResearcherComponent],
  exports: [ResearcherComponent],
})
export class ResearcherModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([open_access]);
  }
}


