import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SearchResearcherComponent} from "./search-researcher.component";
import {SearchResearcherRoutingModule} from "./search-researcher-routing.module";
import {SearchInputModule} from "../../openaireLibrary/sharedComponents/search-input/search-input.module";
import {MatSelectModule} from "@angular/material/select";
import {SearchOrcidServiceModule} from "../../openaireLibrary/claims/claim-utils/service/searchOrcidService.module";
import {SearchResearchResultsServiceModule} from "../../openaireLibrary/services/searchResearchResultsService.module";
import {LoadingModule} from "../../openaireLibrary/utils/loading/loading.module";
import {RouterModule} from "@angular/router";
import {IconsModule} from '../../openaireLibrary/utils/icons/icons.module';



@NgModule({
  declarations: [SearchResearcherComponent],
  imports: [
    CommonModule, SearchResearcherRoutingModule, SearchInputModule, MatSelectModule, SearchOrcidServiceModule, SearchResearchResultsServiceModule, LoadingModule,
    RouterModule, IconsModule

  ]
})
export class SearchResearcherModule { }
