import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {SearchResearcherComponent} from "./search-researcher.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: SearchResearcherComponent
      }
    ])
  ]
})
export class SearchResearcherRoutingModule {
}
