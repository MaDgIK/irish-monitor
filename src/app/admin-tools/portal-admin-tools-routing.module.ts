import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [RouterModule.forChild([
    {path: '', loadChildren: () => import('./admin-tools-routing.module').then(m => m.AdminToolsRoutingModule)},
    {path: 'portals', loadChildren: () => import('../openaireLibrary/dashboard/portal/portals.module').then(m => m.PortalsModule)},
    {path: 'classes', loadChildren: () => import('../openaireLibrary/dashboard/divId/divIds.module').then(m => m.DivIdsModule)}
  ])]
})
export class PortalAdminToolsRoutingModule {

}
