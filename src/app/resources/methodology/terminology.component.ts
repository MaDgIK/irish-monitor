import {ChangeDetectorRef, Component, Inject, PLATFORM_ID} from "@angular/core";
import {Meta, Title} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {SEOService} from "../../openaireLibrary/sharedComponents/SEO/SEO.service";
import {BaseComponent} from "../../openaireLibrary/sharedComponents/base/base.component";
import {PiwikService} from "../../openaireLibrary/utils/piwik/piwik.service";
import {LayoutService} from "../../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {HelperService} from "../../openaireLibrary/utils/helper/helper.service";
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {isPlatformServer} from "@angular/common";

@Component({
  selector: 'terminology',
  templateUrl: 'terminology.component.html',
  styleUrls: ['terminology.component.less']
})
export class TerminologyComponent extends BaseComponent{
  title = 'Terminology & Construction';
  description = 'Terminology & Construction';
  breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'resources - terminology & construction'}];
  tab: 'entities' | 'attributes' = 'entities';
  contentSections: string[] = ['entities', 'inherited-and-inferred-attributes', 'constructed-attributes'];
  activeSection: string;
  divContents: any;

  isMobile: boolean = false;
	isServer: boolean;

  constructor(protected _route: ActivatedRoute,
              protected _piwikService: PiwikService,
              protected _meta: Meta,
              protected seoService: SEOService,
              protected _title: Title,
              protected _router: Router,
              private cdr: ChangeDetectorRef,
              private layoutService: LayoutService,
              private helper: HelperService,
              @Inject(PLATFORM_ID) private platform: any) {
    super();
    this.isServer = isPlatformServer(this.platform);
  }

  ngOnInit() {
    this.setMetadata();
    this.subscriptions.push(this._route.fragment.subscribe(fragment => {
      if(fragment) {
        this.activeSection = fragment;
      } else {
        this.activeSection = 'entities';
      }
    }));
    this.layoutService.isMobile.subscribe(isMobile => {
			this.isMobile = isMobile;
			this.cdr.detectChanges();
		});
    this.getDivContents();
  }

  private getDivContents() {
    this.subscriptions.push(this.helper.getDivHelpContents(this.properties, 'irish', '/methodology/terminology').subscribe(contents => {
      this.divContents = contents;
    }));
  }
}