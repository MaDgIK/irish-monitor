import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {MethodologicalApproachComponent} from "./methodological-approach.component";
import {TerminologyComponent} from "./terminology.component";
import {SliderTabsModule} from "../../openaireLibrary/sharedComponents/tabs/slider-tabs.module";
import {HelperModule} from "../../openaireLibrary/utils/helper/helper.module";
import {BreadcrumbsModule} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";

@NgModule({
  declarations: [MethodologicalApproachComponent, TerminologyComponent],
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      redirectTo: 'methodological-approach',
      pathMatch: 'full',
      canDeactivate: []
    },
    {
      path: 'methodological-approach',
      component: MethodologicalApproachComponent,
      canDeactivate: []
    },
    {
      path: 'terminology',
      component: TerminologyComponent,
      canDeactivate: []
    },
  ]), SliderTabsModule, HelperModule, BreadcrumbsModule],
  exports: []
})
export class MethodologyModule {
}
