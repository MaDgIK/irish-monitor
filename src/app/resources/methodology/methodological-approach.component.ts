import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {BaseComponent} from "../../openaireLibrary/sharedComponents/base/base.component";
import {ActivatedRoute, Router} from "@angular/router";
import {PiwikService} from "../../openaireLibrary/utils/piwik/piwik.service";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../../openaireLibrary/sharedComponents/SEO/SEO.service";
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {LayoutService} from "../../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";

@Component({
  selector: 'methodological-approach',
  templateUrl: 'methodological-approach.component.html',
  styleUrls: ['methodological-approach.component.less']
})
export class MethodologicalApproachComponent  extends BaseComponent implements OnInit {
  title = 'Methodological Approach';
  description = 'Methodological Approach';
  breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'resources - methodological approach'}];

  isMobile: boolean = false;

  constructor(protected _route: ActivatedRoute,
              protected _piwikService: PiwikService,
              protected _meta: Meta,
              protected seoService: SEOService,
              protected _title: Title,
              protected _router: Router,
              private cdr: ChangeDetectorRef,
              private layoutService: LayoutService) {
    super();
  }

  ngOnInit() {
    this.setMetadata();
    this.layoutService.isMobile.subscribe(isMobile => {
			this.isMobile = isMobile;
			this.cdr.detectChanges();
		});
  }
}
