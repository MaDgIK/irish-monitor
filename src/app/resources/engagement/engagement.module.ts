import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {EngagementComponent} from "./engagement.component";
import {RouterModule} from "@angular/router";
import {BreadcrumbsModule} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {HelperModule} from "../../openaireLibrary/utils/helper/helper.module";

@NgModule({
  declarations: [EngagementComponent],
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '', component: EngagementComponent
    }
  ]), BreadcrumbsModule, HelperModule],
  exports: [EngagementComponent]
})
export class EngagementModule {
}
