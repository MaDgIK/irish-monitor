import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {BaseComponent} from "../../openaireLibrary/sharedComponents/base/base.component";
import {PiwikService} from "../../openaireLibrary/utils/piwik/piwik.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../../openaireLibrary/sharedComponents/SEO/SEO.service";
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {LayoutService} from "../../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {HelperService} from "../../openaireLibrary/utils/helper/helper.service";

@Component({
  selector: 'engagement',
  templateUrl: 'engagement.component.html',
  styleUrls: ['engagement.component.less']
})
export class EngagementComponent extends BaseComponent implements OnInit {
  title = 'Engagement & Training';
  description = 'Engagement & Training';
  breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'resources - engagement & training'}];
  divContents: any;

  isMobile: boolean = false;

  constructor(protected _route: ActivatedRoute,
              protected _piwikService: PiwikService,
              protected _meta: Meta,
              protected seoService: SEOService,
              protected _title: Title,
              protected _router: Router,
              private cdr: ChangeDetectorRef,
              private layoutService: LayoutService,
              private helper: HelperService) {
    super();
  }

  ngOnInit() {
    this.setMetadata();
    this.layoutService.isMobile.subscribe(isMobile => {
			this.isMobile = isMobile;
			this.cdr.detectChanges();
		});
    this.getDivContents();
  }

  private getDivContents() {
    this.subscriptions.push(this.helper.getDivHelpContents(this.properties, 'irish', '/engagement-training').subscribe(contents => {
      this.divContents = contents;
    }));
  }
}
