import {Component} from "@angular/core";
import {Meta, Title} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {SEOService} from "../../openaireLibrary/sharedComponents/SEO/SEO.service";
import {BaseComponent} from "../../openaireLibrary/sharedComponents/base/base.component";
import {PiwikService} from "../../openaireLibrary/utils/piwik/piwik.service";
import {HelperService} from "../../openaireLibrary/utils/helper/helper.service";
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";

@Component({
  selector: 'support',
  templateUrl: 'support.component.html',
  styleUrls: ['support.component.less']
})
export class SupportComponent extends BaseComponent {
  title = 'Support';
  description = 'Support';
  breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'resources - support'}];
  pageContents = null;

  constructor(protected _route: ActivatedRoute,
              protected _piwikService: PiwikService,
              protected _meta: Meta,
              protected seoService: SEOService,
              protected _title: Title,
              protected _router: Router,
              private helper: HelperService) {
    super();
  }

  ngOnInit() {
    this.setMetadata();
    this.getPageContents();
  }

  private getPageContents() {
    this.subscriptions.push(this.helper.getPageHelpContents(this.properties, 'irish', this._router.url, 'country').subscribe(contents => {
      this.pageContents = contents;
      console.log(this.pageContents);
    }));
  }
}