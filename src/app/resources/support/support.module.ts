import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SupportComponent} from "./support.component";
import {RouterModule} from "@angular/router";
import {HelperModule} from "../../openaireLibrary/utils/helper/helper.module";
import {BreadcrumbsModule} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";

@NgModule({
  declarations: [SupportComponent],
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '', component: SupportComponent
    }
  ]), HelperModule, BreadcrumbsModule],
  exports: [SupportComponent]
})
export class SupportModule {
}
