import {ChangeDetectorRef, Component} from "@angular/core";
import {BaseComponent} from "../../openaireLibrary/sharedComponents/base/base.component";
import {ActivatedRoute, Router} from "@angular/router";
import {PiwikService} from "../../openaireLibrary/utils/piwik/piwik.service";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../../openaireLibrary/sharedComponents/SEO/SEO.service";
import {LayoutService} from "../../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {HelperService} from "../../openaireLibrary/utils/helper/helper.service";
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";

@Component({
  selector: 'about',
  templateUrl: 'about.component.html',
  styleUrls: ['about.component.less']
})
export class AboutComponent extends BaseComponent {
  title = 'About';
  description = 'About';
  breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'resources - about'}];
  tab: 'mission' | 'context' | 'norf' | 'irel' | 'openaire' = 'mission';
  contentSections: string[] = ['entities', 'inherited-and-inferred-attributes', 'constructed-attributes'];
  activeSection: string;
  divContents: any;

  isMobile: boolean = false;
  isServer: boolean;

  constructor(protected _route: ActivatedRoute,
              protected _piwikService: PiwikService,
              protected _meta: Meta,
              protected seoService: SEOService,
              protected _title: Title,
              protected _router: Router,
              private cdr: ChangeDetectorRef,
              private layoutService: LayoutService,
              private helper: HelperService) {
    super();
  }

  ngOnInit() {
    this.setMetadata();
    this.subscriptions.push(this._route.fragment.subscribe(fragment => {
      if(fragment) {
        this.activeSection = fragment;
      } else {
        this.activeSection = 'mission';
      }
    }));
    this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;
      this.cdr.detectChanges();
    });
  }
}