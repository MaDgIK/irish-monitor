import {ChangeDetectorRef, Component} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {PiwikService} from "src/app/openaireLibrary/utils/piwik/piwik.service";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "src/app/openaireLibrary/sharedComponents/SEO/SEO.service";
import {StakeholderBaseComponent} from "../../openaireLibrary/monitor-admin/utils/stakeholder-base.component";
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {LayoutService} from "../../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";

@Component({
  selector: 'the-five-monitors',
  templateUrl: 'the-five-monitors.component.html',
  styleUrls: ['the-five-monitors.component.less']
})
export class TheFiveMonitorsComponent extends StakeholderBaseComponent{
  title = 'The 5 Monitors';
  description = 'The 5 Monitors';
  breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'resources - the 5 monitors'}];
  public sections: string[] = ['RPOs', 'RFOs', 'Researchers', 'Repositories'];
  public offset: number;
  public shouldSticky: boolean = true;

  isMobile: boolean = false;

  constructor(protected _route: ActivatedRoute,
              protected _piwikService: PiwikService,
              protected _meta: Meta,
              protected seoService: SEOService,
              protected _title: Title,
              protected _router: Router,
              private cdr: ChangeDetectorRef,
              private layoutService: LayoutService) {
    super();
  }

  ngOnInit() {
    this.setMetadata();
    this.layoutService.isMobile.subscribe(isMobile => {
			this.isMobile = isMobile;
			this.cdr.detectChanges();
		});
    this.subscriptions.push(this.layoutService.isBottomIntersecting.subscribe(isBottomIntersecting => {
      this.shouldSticky = !isBottomIntersecting;
      this.cdr.detectChanges();
    }));
  }

  ngAfterViewInit() {
    if (typeof document !== 'undefined') {
      this.offset = Number.parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height'));
      this.cdr.detectChanges();
    }
  }
}
