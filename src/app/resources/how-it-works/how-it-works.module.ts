import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {AboutComponent} from "./about.component";
import {TheFiveMonitorsComponent} from "./the-five-monitors.component";
import {UserActionsComponent} from "./user-actions.component";
import {SliderTabsModule} from "../../openaireLibrary/sharedComponents/tabs/slider-tabs.module";
import {YouWeComponent} from "./you-we.component";
import {BreadcrumbsModule} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {IconsModule} from "../../openaireLibrary/utils/icons/icons.module";
import {HelperModule} from "../../openaireLibrary/utils/helper/helper.module";

@NgModule({
  declarations: [AboutComponent, TheFiveMonitorsComponent, UserActionsComponent, YouWeComponent],
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      redirectTo: 'about',
      pathMatch: 'full',
      canDeactivate: []
    },
    {
      path: 'about',
      component: AboutComponent,
      canDeactivate: []
    },
    {
      path: 'the-5-monitors',
      component: TheFiveMonitorsComponent,
      canDeactivate: []
    },
		{
      path: 'user-actions',
      component: UserActionsComponent,
			canDeactivate: []
    }
  ]), SliderTabsModule, BreadcrumbsModule, IconsModule, HelperModule],
  exports: []
})
export class HowItWorksModule {
}
