import {ChangeDetectorRef, Component, Inject, PLATFORM_ID} from "@angular/core";
import {BaseComponent} from "../../openaireLibrary/sharedComponents/base/base.component";
import {ActivatedRoute, Router} from "@angular/router";
import {PiwikService} from "../../openaireLibrary/utils/piwik/piwik.service";
import {SEOService} from "../../openaireLibrary/sharedComponents/SEO/SEO.service";
import {Meta, Title} from "@angular/platform-browser";
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {isPlatformServer} from "@angular/common";
import {LayoutService} from "../../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {HelperService} from "../../openaireLibrary/utils/helper/helper.service";

@Component({
  selector: 'user-actions',
  templateUrl: 'user-actions.component.html',
  styleUrls: ['user-actions.component.less']
})
export class UserActionsComponent extends BaseComponent{
  title = 'User Actions';
  description = 'User Actions';
  breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'resources - user actions'}];
  activeSection: string;
  divContents: any;

  isMobile: boolean = false;
  isServer: boolean;

  constructor(protected _route: ActivatedRoute,
              protected _piwikService: PiwikService,
              protected _meta: Meta,
              protected seoService: SEOService,
              protected _title: Title,
              protected _router: Router,
              private cdr: ChangeDetectorRef,
              private layoutService: LayoutService,
              private helper: HelperService,
              @Inject(PLATFORM_ID) private platform: any) {
    super();
    this.isServer = isPlatformServer(this.platform);
  }

  ngOnInit() {
    this.setMetadata();
    this.subscriptions.push(this._route.fragment.subscribe(fragment => {
      if(fragment) {
        this.activeSection = fragment;
      } else {
        this.activeSection = 'add-to-orcid';
      }
    }));
    this.layoutService.isMobile.subscribe(isMobile => {
			this.isMobile = isMobile;
			this.cdr.detectChanges();
		});
    this.getDivContents();
  }

  private getDivContents() {
    this.subscriptions.push(this.helper.getDivHelpContents(this.properties, 'irish', '/how-it-works/user-actions').subscribe(contents => {
      this.divContents = contents;
    }));
  }
}