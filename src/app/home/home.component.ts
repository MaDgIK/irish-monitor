import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {BaseComponent} from "../openaireLibrary/sharedComponents/base/base.component";
import {ActivatedRoute, Router} from "@angular/router";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {LayoutService} from "../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";

@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.less']
})
export class HomeComponent extends BaseComponent implements OnInit {
  title: string = 'OA Monitor Ireland';
  description: string = 'OA Monitor Ireland';
  isMobile: boolean = false;

  constructor(
    protected _route: ActivatedRoute,
    protected _piwikService: PiwikService,
    protected _meta: Meta,
    protected seoService: SEOService,
    protected _title: Title,
    protected _router: Router,
    private layoutService: LayoutService,
    private cdr: ChangeDetectorRef
    ) {
      super();
  }

  ngOnInit() {
    this.setMetadata();
    this.subscriptions.push(this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;
      this.cdr.detectChanges();
    }));
  }
}
