import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {HomeComponent} from "./home.component";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {path: '', component: HomeComponent}
  ]), IconsModule, Schema2jsonldModule],
  declarations: [HomeComponent],
  exports: [HomeComponent]
})
export class HomeModule {

}
