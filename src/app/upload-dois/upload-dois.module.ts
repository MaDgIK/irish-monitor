import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UploadDoisComponent} from "./upload-dois.component";
import {UploadDoisRoutingModule} from "./upload-dois-routing.module";
import {SearchResearchResultsServiceModule} from "../openaireLibrary/services/searchResearchResultsService.module";
import {PagingModule} from "../openaireLibrary/utils/paging.module";
import {SearchInputModule} from "../openaireLibrary/sharedComponents/search-input/search-input.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {DropdownFilterModule} from "../openaireLibrary/utils/dropdown-filter/dropdown-filter.module";
import {SearchFilterModule} from "../openaireLibrary/searchPages/searchUtils/searchFilter.module";
import {LogServiceModule} from "../openaireLibrary/utils/log/LogService.module";


@NgModule({
  declarations: [UploadDoisComponent],
  imports: [
    CommonModule, UploadDoisRoutingModule, SearchResearchResultsServiceModule, PagingModule, SearchInputModule, IconsModule, DropdownFilterModule, SearchFilterModule, LogServiceModule

  ],
})
export class UploadDoisModule { }
