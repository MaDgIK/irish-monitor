import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {UploadDoisComponent} from "./upload-dois.component";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: UploadDoisComponent, canActivate: [LoginGuard]
      }
    ])
  ]
})
export class UploadDoisRoutingModule {
}
