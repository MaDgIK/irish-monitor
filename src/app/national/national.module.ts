import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {Route, RouterModule} from "@angular/router";
import {NationalComponent} from "./national.component";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {RoleVerificationModule} from "../openaireLibrary/role-verification/role-verification.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {IconsService} from "../openaireLibrary/utils/icons/icons.service";
import {open_access} from "../openaireLibrary/utils/icons/icons";
import {SandboxGuard} from "../shared/sandbox.guard";
import {OaIndicatorModule} from "../shared/oa-indicator/oa-indicator.module";

const routes: Route[] = [
  {
    path: '', component: NationalComponent, children: [
      {path: 'search', loadChildren: () => import('../search/search.module').then(m => m.SearchModule), data: {activeMenuItem: "national"}, canActivateChild: [SandboxGuard]},
      {path: '', loadChildren: () => import('../shared/monitor/monitor.module').then(m => m.MonitorModule), data: {activeMenuItem: "national"}, canActivateChild: [SandboxGuard]},
    ], data: {stakeholder: 'irish'}
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), LoadingModule, RoleVerificationModule, IconsModule, OaIndicatorModule],
  declarations: [NationalComponent],
  exports: [NationalComponent],
})
export class NationalModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([open_access]);
  }
}
