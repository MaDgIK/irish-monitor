import {Component, OnInit} from "@angular/core";
import {Stakeholder} from "../openaireLibrary/monitor/entities/stakeholder";
import {StakeholderService} from "../openaireLibrary/monitor/services/stakeholder.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {Meta, Title} from "@angular/platform-browser";
import {SearchCustomFilter} from "../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {LinksResolver} from "../search/links-resolver";
import {CustomFilterService} from "../openaireLibrary/shared/customFilter.service";
import {StakeholderBaseComponent} from "../openaireLibrary/monitor-admin/utils/stakeholder-base.component";
import {LayoutService} from "../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {OpenaireEntities} from "../openaireLibrary/utils/properties/searchFields";
import {ConfigurationService} from "../openaireLibrary/utils/configuration/configuration.service";
import {Session} from "../openaireLibrary/login/utils/helper.class";
import {properties as beta} from "../../environments/environment.beta";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";

@Component({
  selector: 'national',
  template: `
      <div>
          <loading *ngIf="loading" class="uk-position-center"></loading>
          <div *ngIf="!loading && stakeholder">
              <div class="uk-banner uk-light">
                  <div class="uk-container uk-container-large">
                      <div class="uk-padding-small uk-padding-remove-vertical">
                          <div class="uk-grid uk-grid-large uk-flex-middle uk-margin-medium-bottom" uk-grid>
                              <div class="uk-width-expand">
                                  <h1 class="uk-h3 uk-margin-remove uk-text-truncate">{{ entities.country }} {{ entities.stakeholder }}</h1>
                              </div>
                              <div class="uk-width-auto">
                                  <oa-indicator [stakeholder]="stakeholder"></oa-indicator>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="uk-banner-footer">
                      <div class="uk-container uk-container-large uk-flex uk-flex-between">
                          <ul class="uk-banner-tab uk-padding-small uk-padding-remove-vertical">
                              <li [class.uk-active]="!isSearch">
                                  <a [routerLink]="'/national'">{{ entities.stakeholder }}</a>
                              </li>
                              <li [class.uk-active]="isSearch">
                                  <a [routerLink]="'/national/search/find/research-outcomes'"
                                     [queryParams]="routerHelper.createQueryParams(['type','peerreviewed'], [quote('publications'), quote('true')])">
                                      Browse {{ openaireEntities.RESULTS }}</a>
                              </li>
                          </ul>
                          <div *ngIf="!isMobile" class="uk-margin-large-right uk-flex uk-flex-middle">
                              <a *ngIf="isManager" [routerLink]="adminLink" target="_blank"
                                 class="uk-button uk-flex uk-flex-middle uk-margin-small-right">
                                  Manage
                              </a>
                              <a *ngIf="sandboxLink" [href]="sandboxLink" target="_blank"
                                 class="uk-button uk-flex uk-flex-middle uk-margin-small-right">
                                  Sandbox
                              </a>
                          </div>
                      </div>
                  </div>
              </div>
              <div>
                  <router-outlet></router-outlet>
              </div>
          </div>
          <h3 *ngIf="!loading && !stakeholder" class="uk-position-center">
              No {{ entities.country }} {{ entities.stakeholder }} yet.
          </h3>
      </div>
      <role-verification *ngIf="stakeholder" [id]="stakeholder.alias" [name]="stakeholder.name"
                         [dashboard]="'National Open Access Monitor, Ireland'"
                         [type]="stakeholder.type" [service]="'irish'" [relativeTo]="null"></role-verification>
  `
})
export class NationalComponent extends StakeholderBaseComponent implements OnInit {
  stakeholder: Stakeholder;
  loading: boolean = true;
  isMobile: boolean = false;
  isSearch: boolean = false;
  openaireEntities = OpenaireEntities;

  constructor(private stakeholderService: StakeholderService,
              private userManagementService: UserManagementService,
              private layoutService: LayoutService,
              protected _router: Router,
              protected _route: ActivatedRoute,
              protected seoService: SEOService,
              protected _piwikService: PiwikService,
              protected _title: Title,
              protected _meta: Meta,
              private _customFilterService:CustomFilterService,
              private configurationService: ConfigurationService) {
    super();
    super.initRouterParams(this._route, event => {
      this.isSearch = event.url.includes('search');
    });
  }

  ngOnInit() {
    this.layoutService.setRootClass('country');
    this.subscriptions.push(this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;
    }));
    this.stakeholderService.getStakeholder(this._route.snapshot.data.stakeholder).subscribe(stakeholder => {
      this.stakeholder = stakeholder;
      if(this.stakeholder) {
        this.setProperties(this.stakeholder.alias, this.stakeholder.type, this.configurationService);
        this._customFilterService.setCustomFilter([new SearchCustomFilter("National", "country", "IE", "Irish National Monitor")]);
        LinksResolver.resetProperties();
        this.loading = false;
      } else {
        this.loading = false;
      }
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.layoutService.setRootClass(null);
  }

  get isManager() {
    return Session.isPortalAdministrator(this.userManagementService.user) ||
        Session.isCurator(this.stakeholder.type, this.userManagementService.user) ||
        Session.isManager(this.stakeholder.type, this.stakeholder.alias, this.userManagementService.user);
  }

  get isMember() {
    return this.isManager ||
        Session.isMember(this.stakeholder.type, this.stakeholder.alias, this.userManagementService.user);
  }

  get adminLink() {
    return "/admin/" + this.stakeholder.alias;
  }

  get sandboxLink() {
    if(this.properties.environment !== 'beta' && this.isMember) {
      return beta.domain + '/national/';
    } else {
      return null;
    }
  }
}
