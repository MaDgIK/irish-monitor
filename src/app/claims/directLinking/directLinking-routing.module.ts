import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {OpenaireDirectLinkingComponent} from './directLinking.component';
import {LoginGuard} from '../../openaireLibrary/login/loginGuard.guard';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {properties} from "../../../environments/environment";


@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: OpenaireDirectLinkingComponent, canActivate: [LoginGuard], data: {
          redirect: properties.errorLink,  community : 'openaire'
        }, canDeactivate: [PreviousRouteRecorder]},

    ])
  ]
})
export class DirectLinkingRoutingModule { }
