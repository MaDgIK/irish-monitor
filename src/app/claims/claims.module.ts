import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {Route, RouterModule} from "@angular/router";

const routes: Route[] = [
  {path: 'claim', loadChildren: () => import('./linking/linkingGeneric.module').then(m => m.LibLinkingGenericModule)},
  {path: 'direct-claim', loadChildren: () => import('./directLinking/directLinking.module').then(m => m.LibDirectLinkingModule)},
  {path: 'myclaims', loadChildren: () => import('./myClaims/myClaims.module').then(m => m.LibMyClaimsModule)}
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],

})
export class ClaimsModule {
}
