import {Component, OnInit} from '@angular/core';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-claims-admin',
  template: `
    <claims-admin *ngIf="claimsInfoURL && userInfoURL" [claimsInfoURL]="claimsInfoURL">
    </claims-admin>
  `,
})
export class OpenaireClaimsAdminComponent implements OnInit {
  claimsInfoURL: string;
  userInfoURL: string;
  
  constructor() {
  }
  
  public ngOnInit() {
    this.claimsInfoURL = properties.claimsInformationLink;
  }
}
