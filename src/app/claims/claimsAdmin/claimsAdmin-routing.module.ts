import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { OpenaireClaimsAdminComponent } from './claimsAdmin.component';
import{  ClaimsCuratorGuard} from '../../openaireLibrary/login/claimsCuratorGuard.guard';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from '../../openaireLibrary/error/isRouteEnabled.guard';
import {properties} from "../../../environments/environment";


@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: OpenaireClaimsAdminComponent, canActivate: [IsRouteEnabled, ClaimsCuratorGuard],
      data: {redirect: properties.errorLink,  community : 'openaire'}, canDeactivate: [PreviousRouteRecorder]}])
  ]
})
export class ClaimsAdminRoutingModule { }
