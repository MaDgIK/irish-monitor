import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {FormsModule} from "@angular/forms";
import {OrcidModule} from "../openaireLibrary/orcid/orcid.module";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";
import {Route, RouterModule} from "@angular/router";
import {OrcidComponent} from "../openaireLibrary/orcid/orcid.component";

const routes: Route[] = [
  {
    path: '', component:  OrcidComponent
  }
];

@NgModule({
  imports: [
    CommonModule, FormsModule,
    OrcidModule, RouterModule.forChild(routes)
  ],
  providers: [PreviousRouteRecorder, LoginGuard],
})
export class LibOrcidModule { }
