import {Component} from '@angular/core';

@Component({
  selector: 'openaire-my-orcid-links',
  template: `
      <div class="uk-section">
          <my-orcid-links></my-orcid-links>
      </div>
  `
})

export class OpenaireMyOrcidLinksComponent {

  constructor() {}

  public ngOnInit() {}
}

