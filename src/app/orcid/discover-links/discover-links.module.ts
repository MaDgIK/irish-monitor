import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DiscoverLinksComponent} from "./discover-links.component";
import {RouterModule} from "@angular/router";
import {LoginGuard} from "../../openaireLibrary/login/loginGuard.guard";
import {SearchResearchResultsModule} from "../../openaireLibrary/searchPages/searchResearchResults.module";
import {SearchInputModule} from "../../openaireLibrary/sharedComponents/search-input/search-input.module";


@NgModule({
  declarations: [DiscoverLinksComponent],
  imports: [
    CommonModule, RouterModule.forChild([
      {
        path: '',
        component: DiscoverLinksComponent, canActivate: [LoginGuard]
      }
    ]), SearchResearchResultsModule, SearchInputModule

  ],
})
export class DiscoverLinksModule { }
