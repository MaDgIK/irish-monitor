import {Component, OnInit} from '@angular/core';
import {SearchResearchResultsService} from "../../openaireLibrary/services/searchResearchResults.service";
import {LogService} from "../../openaireLibrary/utils/log/log.service";
import {ActivatedRoute, Router} from '@angular/router';
import {SEOService} from '../../openaireLibrary/sharedComponents/SEO/SEO.service';
import {PiwikService} from '../../openaireLibrary/utils/piwik/piwik.service';
import {Meta, Title} from '@angular/platform-browser';
import {UserManagementService} from "../../openaireLibrary/services/user-management.service";
import {SearchCustomFilter} from "../../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {SearchBaseComponent} from "../../search/searchPages/searchBaseComponent";

@Component({
  selector: 'discover-links',
  templateUrl: './discover-links.component.html'
})
export class DiscoverLinksComponent extends  SearchBaseComponent  implements OnInit {
  filters = null;
  user = null;
  constructor(private _searchResearchResultsService: SearchResearchResultsService,
              private _logService: LogService,
              protected router: Router,
              protected route: ActivatedRoute,
              protected seoService: SEOService,
              protected _piwikService: PiwikService,
              protected _title: Title,
              protected _meta: Meta, private _userManagementService: UserManagementService) {
    super();
  }

  ngOnInit() {
    this.title = 'Discover links';
    this.description = 'Discover links';
    this.setMetadata();

    this.subscriptions.push(this._userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
      this.filters = [new SearchCustomFilter("Author", "resultauthor", user.fullname, user.fullname, false, 'resultauthor="'+user.firstname + '" and resultauthor="'+ user.lastname+'"')];


    }));
  }

}
