import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PublicLogsComponent} from "./public-logs.component";
import {LogServiceModule} from "../openaireLibrary/utils/log/LogService.module";
import {Route, RouterModule} from "@angular/router";
import {BreadcrumbsModule} from '../openaireLibrary/utils/breadcrumbs/breadcrumbs.module';

const routes: Route[] = [
  {
    path: '', component: PublicLogsComponent
  }
];


@NgModule({
  declarations: [PublicLogsComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes),  LogServiceModule, RouterModule, BreadcrumbsModule
  ],
  exports: [PublicLogsComponent]
})
export class PublicLogsModule { }
