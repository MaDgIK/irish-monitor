import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '../openaireLibrary/sharedComponents/base/base.component';
import {ActivatedRoute, Router} from '@angular/router';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {Meta, Title} from '@angular/platform-browser';
import {LogService} from "../openaireLibrary/utils/log/log.service";
import {Breadcrumb} from '../openaireLibrary/utils/breadcrumbs/breadcrumbs.component';

@Component({
  selector: 'public-logs',
  template: `
    <div class="uk-banner dark uk-padding-remove-vertical uk-light">
      <div class="uk-container uk-container-large uk-section uk-section-small">
        <div>
          <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
        </div>
        <div class="uk-text-center uk-margin-medium-top" uk-scrollspy-class>
          <h1 class="uk-h2 uk-margin-medium-top uk-margin-medium-bottom">Web Statistics & Activity Logs</h1>
        </div>
      </div>
    </div>
    <div class="uk-container uk-container-small uk-section">
      <ul class="uk-tab uk-flex uk-flex-center uk-flex-middle uk-flex-nowrap uk-text-large" uk-switcher>
        <li><a href="#">Web statistics</a></li>
        <li><a href="#">Monitor logs</a></li>
        <li><a href="#">OpenOrgs logs</a></li>
      </ul>
      <ul class="uk-switcher uk-margin-medium-top">
        <li>
          <div>Explore analytics detailing the user engagement on the National Monitor. This section offers insights into traffic patterns, visitor demographics, page views, and other key metrics that help understand National Monitor usage and effectiveness.</div>
          <div class="uk-alert uk-alert-warning">Current month web statistics will be available at the beginning of next month!</div>
          <ul class="uk-list uk-margin-top">
            <ng-container *ngFor="let year of years ">
              <ng-container *ngFor="let month of months ">
                <ng-container *ngIf="show(year, month) ">
                <li *ngIf=" !(year == today.getFullYear()  &&  month == today.getMonth()+1)">
                    <a [href]="properties.logServiceUrl + 'webstats/' + year + '/' + (month<10?'0'+month:month) " target="_blank"> {{month}}/{{year}}</a>
                  </li>
                </ng-container>
              </ng-container>
            </ng-container>
          </ul>
        </li>
        <li>
          <div>

            Access monthly logs to see user activities such as uploading DOIs for metadata review, claiming links to one’s ORCID profile, and linking research outputs. To understand the specifics of these activities, visit 
            <a routerLink="/user-actions">User Actions</a>. These logs offer a clear view of user interactions within the open access ecosystem.
            
          </div>

          <ul class="uk-list  uk-margin-top">
            <ng-container *ngFor="let year of years ">
              <ng-container *ngFor="let month of months ">
                <ng-container *ngIf="show(year, month) "> 
                  <li>
                    <a [href]="properties.logServiceUrl + 'public-logs/' + year + '/' + month " target="_blank"> {{month}}/{{year}}</a>
                  </li>
                </ng-container>
              </ng-container>
            </ng-container>
          </ul>        </li>
        
        <li>
          <div>    Review monthly logs from <a [href]="properties.openOrgsUrl" target="_blank">OpenOrgs</a>, focusing on the identification and disambiguation of Irish Research Performing Organisations (RPOs) 
            within the <a href="https://graph.openaire.eu" target="_blank">OpenAIRE Graph</a>. Access is exclusive to the primary RPO dashboard managers and IReL representatives, ensuring consistent and accurate information management. Each log provides a clear overview of a month's updates and changes in OpenOrgs.
            <a routerLink="/contact-us">Contact us</a> if you would like to apply to be a manager for your RPO.  </div>
          <ul class="uk-list  uk-margin-top">
            <ng-container *ngFor="let year of years ">
              <ng-container *ngFor="let month of months ">
                <ng-container *ngIf="show(year, month) ">                  <li>
                    <a [href]="properties.openOrgsUrl + '/public-api/logs/' + year + '/' + month+ '/IE' " target="_blank"> {{month}}/{{year}}</a>
                  </li>
                </ng-container>
              </ng-container>
            </ng-container>
          </ul>
        </li>
        
      </ul>
    </div>
    <ng-template #formattedLogs let-logs="logs">
      <div *ngIf="logs && logs.length > 0" class="uk-text-meta">Viewing last  {{logs.length}} actions.</div>
      <div *ngIf="!logs || logs.length == 0" class="uk-text-meta">No logs yet.</div>
      <div *ngFor="let log of logs">
        On {{log.date | date: 'd/M/yyyy, hh:mm':'GMT'  }} {{log.message}}
      </div>
    </ng-template>
    
  `
})
export class PublicLogsComponent extends BaseComponent implements OnInit {
  breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'resources - web statistics & activity logs'}];
  isConsent = false;
  redirectUrl = null;
  startDate = new Date();
  today = new Date();
  months = [];
  years = [];
  constructor(protected _router: Router,
              protected _route: ActivatedRoute,
              protected seoService: SEOService,
              protected _piwikService: PiwikService,
              protected _title: Title,
              protected _meta: Meta,
              private _logService: LogService) {
    super();
    this.startDate.setFullYear(2024,0,10)
    this.months = Array.from({ length:12  }, (_, index) => 12 - index);
    this.years = Array.from({ length:this.today.getFullYear() + 1 - this.startDate.getFullYear()  }, (_, index) => this.today.getFullYear() - index);
  }

  ngOnInit() {
    this.title = 'Public logs';
    this.description = 'Public logs of user actions in OA monitor - Ireland';
    this.setMetadata();
  }
  
  show(year, month){
    return (year > this.startDate.getFullYear() && year < this.today.getFullYear()) ||
      (year == this.startDate.getFullYear() && year < this.today.getFullYear() && month >= this.startDate.getMonth() + 1 ) ||
      (year > this.startDate.getFullYear() && year == this.today.getFullYear() && month <= this.today.getMonth() + 1 ) ||
      (year == this.startDate.getFullYear() && year == this.today.getFullYear() && month >= this.startDate.getMonth() + 1 && month <= this.today.getMonth()+1 ) ||
      ( year == this.today.getFullYear()  && month < this.today.getMonth()+1 )
  }
}
