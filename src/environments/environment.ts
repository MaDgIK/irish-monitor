// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonDev} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  dashboard: "irish",
  adminToolsAPIURL: "http://mpagasas.di.uoa.gr:19780/irish-monitor-service/",
  monitorServiceAPIURL: "http://mpagasas.di.uoa.gr:19780/irish-monitor-service/",
  monitorStatsFrameUrl:"https://stats.madgik.di.uoa.gr/stats-api/",
  searchOrcidURL: "https://pub.orcid.org/v3.0/",
  piwikSiteId: "407",
  enablePiwikTrack:false,
  piwikBaseUrl: 'https://beta.analytics.openaire.eu/piwik.php?idsite=',
  disableFrameLoad: true,
  adminToolsPortalType: 'country',
  adminToolsCommunity: 'irish',
  domain: "http://mpagasas.di.uoa.gr:5500",
  zenodoDumpUrl: "https://doi.org/10.5281/zenodo.10474106",
  logServiceUrl: "http://scoobydoo.di.uoa.gr:4200/",
  logFilesPath: "/var/log/irish-log/",
  matomoLogFilesPath: "/var/log/irish-log/",

  myClaimsLink:"/participate/myclaims",
  loginServiceURL: ["http://mpagasas.di.uoa.gr:19780/irish-monitor-service/", <string>commonDev.loginServiceURL],
  afterLoginRedirectLink: "/user-policy",
  helpdeskEmail: 'oamonitor.ireland@openaire.eu',
  useHelpTexts: true,
  orcidDiscoverLinksPage: "/orcid-discover-links"
}

export let properties: EnvProperties = {
  ...common, ...commonDev, ...props
}
